<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class SentinelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
           'id'         =>  '1',
           'email'      =>  'admin@email.com',
           'password'   =>  '$2y$10$jfkrwf81.XpTF6jjpMJXhuqocDMtphpqBEqVG/pJ0ng32E1NoTCb6',
           'first_name' =>  'Administrator',
           'last_name'  =>  'User',
           'created_at' =>  '2016-03-27 17:19:38',
           'updated_at' =>  '2016-03-27 17:19:38',
        ]);

        DB::table('roles')->insert([
            'id'            =>  '1',
            'slug'          =>  'subscribers',
            'name'          =>  'Subscribers',
            'permissions'   =>  '{"user.create":false,"user.view":true,"user.delete":false,"user.update":true}',
            'created_at'    =>  '2016-03-27 03:37:46',
            'updated_at'    =>  '2016-03-27 17:07:19'
        ]);

        DB::table('roles')->insert([
            'id'            =>  '2',
            'slug'          =>  'administrators',
            'name'          =>  'Administrators',
            'permissions'   =>  '{"user.create":true,"user.view":true,"user.delete":true,"user.update":true}',
            'created_at'    =>  '2016-03-27 03:37:46',
            'updated_at'    =>  '2016-03-27 17:07:19'
        ]);

        DB::table('roles')->insert([
            'id'            =>  '3',
            'slug'          =>  'inactive',
            'name'          =>  'Inactive',
            'permissions'   =>  '{"user.create":false,"user.view":false,"user.delete":false,"user.update":false}',
            'created_at'    =>  '2016-03-27 03:37:46',
            'updated_at'    =>  '2016-03-27 17:07:19'
        ]);

        DB::table('role_users')->insert([
           'user_id'    =>  '1',
           'role_id'    =>  '1',
           'created_at' =>  '2016-03-27 04:08:17',
           'updated_at' =>  '2016-03-27 04:08:17'
        ]);

        DB::table('role_users')->insert([
            'user_id'    =>  '1',
            'role_id'    =>  '2',
            'created_at' =>  '2016-03-27 04:08:17',
            'updated_at' =>  '2016-03-27 04:08:17'
        ]);

        DB::table('activations')->insert([
           'id'             =>  '1',
           'user_id'        =>  '1',
           'code'           =>  '0UTqZQgzaT1voslUhMH059PPI99WwuHb',
           'completed'      =>  '1',
           'completed_at'   =>  '2016-03-27 17:40:28',
           'created_at'     =>  '2016-03-27 17:40:28',
           'updated_at'     =>  '2016-03-27 17:40:28'
        ]);

        DB::table('user_timezones')->insert([
            'user_id'    =>  '1',
            'name'       =>  'UTC'
        ]);
    }
}
