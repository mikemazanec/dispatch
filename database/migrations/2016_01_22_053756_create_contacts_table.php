<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // The Sentinel users table primary key needs to unsigned attribute for the foreign key in the contacts table;
        //Schema::table('users', function (Blueprint $table) {
        //    $table->increments('id')->unsigned()->change();
        //});

        Schema::create('contacts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->integer('user_id');
            $table->string('email');
          //  $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Remove the unsigned attribute for the users column;
        //Schema::table('users', function (Blueprint $table) {
        //    $table->increments('id')->change();
        //});

        Schema::drop('contacts');
    }
}
