<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->uuid('id');
            $table->binary('subject');
            $table->binary('body');
            $table->binary('message_key');
            $table->string('parent_message', 36);
            $table->boolean('read_receipt');
            $table->string('reference', 8);
            $table->enum('state', ['draft','active','revoked']);
            $table->timestamps();
            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
