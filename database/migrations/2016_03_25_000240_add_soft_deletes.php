<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSoftDeletes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('messages', function ($table) {
            $table->softDeletes();
            $table->dropColumn('deleted');
        });

        Schema::table('files', function ($table) {
            $table->softDeletes();
        });

        Schema::table('recipients', function ($table) {
            $table->softDeletes();
            $table->dropColumn('deleted');
        });

        Schema::table('retrievals', function ($table) {
            $table->softDeletes();
        });

        Schema::table('senders', function ($table) {
            $table->softDeletes();
            $table->dropColumn('deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('messages', function ($table) {
            $table->boolean('deleted');
        });

        Schema::table('recipients', function ($table) {
            $table->boolean('deleted');
        });

        Schema::table('senders', function ($table) {
            $table->boolean('deleted');
        });
    }
}
