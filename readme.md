# Dispatch

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/eeb912db-22c7-4dee-90bb-fc00814b4502/mini.png)](https://insight.sensiolabs.com/projects/eeb912db-22c7-4dee-90bb-fc00814b4502)

Dispatch is a web application that provides secure message and file transfer between users. **This project has not been audited for vulnerabilities, so use at your own risk**. 

Features:

* Message and file encryption via [Paragonie/Halite](https://github.com/paragonie/halite)
* Automatic message expiration & removal
* Nightly backups to Dropbox
* Message delivery via your preferred e-mail service
* Read receipts
* Optional user registration by invite
* Per-message activity log
* and more!

## Server Requirements
* PHP >= 5.5.9
* MySQL Server
* libsodium (via apt and pecl - see https://paragonie.com/book/pecl-libsodium/read/00-intro.md#installing-libsodium)
* composer
* supervisord (see https://laravel.com/docs/5.2/queues#supervisor-configuration)

## Git & Composer
1. git clone https://gitlab.com/mikemazanec/dispatch.git dispatch
2. Update permissions as necessary
3. git config core.fileMode false
4. cp .env.example .env
5. composer Install

## Artisan
Run the following on the command line:

1. php artisan vendor:publish --provider="Cartalyst\Sentinel\Laravel\SentinelServiceProvider"
2. php artisan key:generate and copy it to .env
3. php artisan migrate

## Configure Backup
* Configure backup settings in config/laravel-backup.php - add dropbox disk settings
* Add your Dropbox API token and secret to .env

## NPM, Bower and Gulp
Run the following on the command line:

1. npm install
2. sudo npm install -g bower (unnecessary for homestead)
3. sudo npm install -g gulp  (unnecessary for homestead)
4. bower install
5. gulp

## Seed the Database

php artisan db:seed --class=SentinelSeeder

## Generate the Symmetric Encryption Key
This is the symmetric encryption key - back it up in a safe place or risk data loss!

php artisan generatesymkey

## Configure supervisord
1. Add the scheduler cron job. see https://laravel.com/docs/5.2/scheduling
2. copy ./laravel-woked.conf.example to /etc/supervisor/conf.d/laravel-worker.conf and update artisan and log paths
3. Start supervisor processes:
  1. sudo supervisorctl reread
  2. sudo supervisorctl update
  3. sudo supervisorctl start laravel-worker:*

## Configure E-Mail Service
Modify .env to use the necessary e-mail service

## Administrator Credentials
Administrator user:admin@email.com pw:welcome99

## Packages Used For This Project
* cartalyst/sentinel
* laravelcollective/html
* nesbot/carbon
* paragonie/halite
* webpatser/laravel-uuid
* league/flysystem-dropbox
* spatie/laravel-backup