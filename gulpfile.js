var elixir = require('laravel-elixir');
//var concat = require('gulp-concat');
//var copy = require('gulp-copy');


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
    'dev': {
        'less': './resources/assets/less/',
        'vendor': './resources/assets/vendor/',
        'app': './resources/assets/app/'
    },
    'production': {
        'css': './public/css/',
        'js': './public/js/',
        'fonts': './public/fonts/'
    }
};

elixir(function(mix) {
    //mix.less('app.less');

    // CSS
    mix.copy(paths.dev.vendor + 'foundation-sites/dist/*.css', paths.production.css);
    mix.copy(paths.dev.vendor + 'entypo-plus/css/*.css', paths.production.css);
    mix.copy(paths.dev.vendor + 'select2/dist/css/*.css', paths.production.css);
    mix.copy(paths.dev.app + 'css/*.css', paths.production.css);

    // Fonts
    mix.copy(paths.dev.vendor + 'entypo-plus/fonts/*.eot', paths.production.fonts);
    mix.copy(paths.dev.vendor + 'entypo-plus/fonts/*.svg', paths.production.fonts);
    mix.copy(paths.dev.vendor + 'entypo-plus/fonts/*.ttf', paths.production.fonts);
    mix.copy(paths.dev.vendor + 'entypo-plus/fonts/*.woff', paths.production.fonts);

    // Javascript
    mix.copy(paths.dev.vendor + 'foundation-sites/dist/*.js', paths.production.js);
    mix.copy(paths.dev.vendor + 'foundation-sites/js/*.js', paths.production.js);
    mix.copy(paths.dev.vendor + 'jquery/dist', paths.production.js);
    mix.copy(paths.dev.vendor + 'select2/dist/js/*.js', paths.production.js);
    mix.copy(paths.dev.app + 'js/*.js', paths.production.js);
    

});
