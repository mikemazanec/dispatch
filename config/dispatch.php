<?php

return [

    /**
     * The number of days after the message was sent that the message will
     * be available, after which, it will become inaccessible.
     */
    'expiration_days' =>  14,

    /**
     * Address to use as the message sender
     */
    'sender' => 'noreply@bearpaw.local',

    /**
     * Maximum file upload size. Default 20MB
     */
    'max_file_size'   =>  '20971520',

    /**
     * List of acceptable file mimes for upload
     */
    'accepted_mimes'    =>  'doc,docx,xls,xlsx,txt,csv,pdf,zip',

    /**
     * Maximum total upload size. Default 50MB
     */
    'max_upload_total_size' =>  '52428800',

    /**
     * Address to send user registration notifications to.
     */
    'admin'	=>	'admin@bearpaw.local',

    /**
     * Name of the organization
     */
    'org_name'  =>  'Bear Paw Development Corporation',

    /**
     * Directory in storage_path() to save encrypted files to. This can be changed to point to any location on the disk.
     */
    'storage_path'   =>  storage_path('app'),

    /**
     * Save message recipient addresses to the contacts table (essentially an address book)
     */
    'save_to_contacts'  =>  true,

    'roles' =>  array(
        'subscribers'       =>  'Subscribers',
        'administrators'    =>  'Administrators',
        'inactive'          =>  'Inactive'
    ),

    /**
     * Allow ANYONE to register.
     * Default: false - user e-mail addresses must be added to the whitelist prior to registration
     *          true - any e-mail address will be accepted for registration
     */
    'allow_open_registration'   => false,

    /**
     * Job run times
     */
    'garbage_collection_time' => '00:00',
    'backup_run_time'   =>  '02:00',
    'backup_clean_time' =>  '01:00',

    /**
     * Halite (Symmetric) Encryption Key Path
     */
    'encryption_key_path' => storage_path('app')."/".env('ENCRYPTION_KEY_PATH'),

    /**
     * CAN-SPAM contact information used in e-mails
     */
    'org_name'      =>  '',
    'org_address'   =>  '',
    'org_city'      =>  '',
    'org_state'     =>  '',
    'org_zipode'    =>  ''
];