<html>
    <head>
        <title>Dispatch - @yield('title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width">

        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <link href="{{asset('css/normalize.css')}}" rel="stylesheet">
        <link href="{{asset('css/foundation.css')}}" rel="stylesheet">
        <link href="{{asset('css/entypo-plus.css')}}" rel="stylesheet">
        @if( isset($assets) && array_key_exists('css', $assets) && count($assets['css']) > 0)
            @foreach($assets['css'] as $asset_path)
                <link href="{{asset($asset_path)}}" rel="stylesheet">
            @endforeach
        @endif

        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/foundation.core.js')}}"></script>
        <script src="{{asset('js/foundation.tooltip.js')}}"></script>
        <script src="{{asset('js/foundation.util.mediaQuery.js')}}"></script>
        @if( isset($assets) &&  array_key_exists('js', $assets) && count($assets['js']) > 0)
            @foreach($assets['js'] as $asset_path)
                <script src="{{asset($asset_path)}}"></script>
            @endforeach
        @endif
        <script src="{{asset('js/app.js')}}"></script>
        <script type="text/javascript">
            $(function () {
                $(document).foundation();
            });
        </script>
    </head>
    <body>
    <div style="min-height:100%;">
        <div id="id_wrapper">
            <div class="row">
                <div class="small-12 large-offset-5 large-7 columns " style="padding-top:8px; color:#fff !important; text-align:right !important" id="identity_container">
                    @include('layouts.identity')
                </div>
            </div>
        </div>
        <div id="banner_wrapper">
            <div class="row">
                <a href='/' class="small-6 large-6 columns"><!-- img src="/img/mus-logo.png" style="margin-top:8px; float:left;" alt="MUS Logo" --></a>
                <h1 class="small-6 large-6 columns hide-for-small-only" style="text-align:right; color:#fff !important; display:inline-block; font: 'Droid Sans', Arial">Dispatch</h1>
            </div>
        </div>
        <div class="container end" style="padding:10px 0 20px 0;" id="content">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>
    <footer style="height:30px; margin-top:-30px; position:relative;">
        <p>&copy; {{ $org_name }}</p>
    </footer>
    </body>
</html>