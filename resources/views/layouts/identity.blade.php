
    <ul id="nav" class="drop">
        @if($user = \Sentinel::check())
            <li><i class="icon-user" style="opacity: 1.0 !important;"></i> <a href="/auth/profile">{{ $user->first_name }} {{ $user->last_name }}</a></li>
            <li><a href="/inbox">Inbox</a></li>
            @if(\Sentinel::inRole('administrators'))
            <li><a href="/admin">Administration</a></li>
            @endif
            <li><a href="/auth/logout">Sign Out</a></li>
        @else
            <li><a href="/auth/login">Sign In</a></li>
            <li><a href="/auth/register">Register</a></li>
        @endif
    </ul>