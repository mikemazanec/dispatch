<p>Below is a list of all users in each of the different role groups. Click a user name to view the user record.</p>
@foreach($roles as $role)
    <?php
    switch($role->name)
    {
        case "Subscribers":
            $slug = "promote";
            break;

        case "Administrators":
            $slug = "demote";
            break;

        case "Inactive Users":
            $slug = "inactive";
            break;
    }
    ?>

    <div class="row prepend-top">
        <div class="large-6 end"><h3>{{ $role->name }}</h3></div>
    </div>
    <div class="row">
        <table>
            <thead>
            <tr>
                <th>User Name</th>
                <th>Last Login</th>
                <th>Last Update</th>
                <th>Status</th>
                <th>{{ ucwords($slug) }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($role->users as $user)
                <tr>
                    <td>{{ $user->last_name }}, {{ $user->first_name }}</td>
                    <td>{{ $user->created_at->timezone($user_timezone) }}</td>
                    <td>{{ $user->updated_at->timezone($user_timezone) }}</td>
                    <td>{{ (\Activation::completed($user) && $user->id > 1 ? link_to('admin/deactivate/'.$user->id, 'Deactivate') : ($user->id > 1 ? link_to('admin/activate/'.$user->id, 'Activate') : "Super User")) }}</td>
                    <td>
                        @if( $user->id > 1)
                            <a href="/admin/{{ $slug }}/{{ $user->id }}">{{ ucwords($slug) }}</a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endforeach