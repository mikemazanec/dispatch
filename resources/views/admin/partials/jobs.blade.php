@if(!empty($failed_jobs))
<p>The following jobs are currently listed as having failed. All jobs can be either delete or requeued, or on an individual basis. Job payloads can be viewed by clicking on the links below. The job payload can be used to debug a process or notify users of an error delivering mail or processing file uploads.</p>
<ul class="accordion" data-accordion data-multi-expand="true">
    @foreach($failed_jobs as $job)
    <?php
    $job_json = json_decode($job->payload);
    ?>
    <li class="accordion-item" data-accordion-item>
        <a class="accordion-title">
            Job {{ $job->id }} ({{ $job_json->job }}) failed at {{ $job->failed_at }}
        </a>
        <div class="accordion-content" data-tab-content>
            <strong>Job Payload:</strong><br />
            {{ $job->payload }}<br /><br />
            <a href="/admin/requeue_job/{{ $job->id }}">Re-Queue Job</a> OR  <a href="/admin/delete_job/{{ $job->id }}">Delete Job</a>
        </div>
    </li>
    @endforeach
</ul>

<a href="/admin/requeue_failed_jobs">Re-Queue ALL Failed Jobs</a><br />
<a href="/admin/delete_failed_jobs">Delete ALL Failed Jobs</a>
@else
<p>No failed jobs were found.</p>
@endif

