<p>All messages will expire and be removed after {{ $expiration_days }} days. The garbage collection process performs this removal, deleting all expired messages and associated files. It is performed once every day at {{ Carbon\Carbon::createFromFormat('H:i', config('dispatch.garbage_collection_time'))->format('H:ia') }}. The process can also be executed manually.</p>

<h4>Perform Garbage Collection</h4>
<p>Garbage collection can be run to free up drive space and remove old, expired messages from the server. The process can be queued manually by <a href="/admin/queue_garbage_collection">clicking here</a>. {{ $expired_message_count }} messages will be removed when the process is run.</p>

