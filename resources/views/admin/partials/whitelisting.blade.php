
@if(config('dispatch.allow_open_registration') == false)
    <div class="large-12 columns callout secondary">
        <h4>Add Address</h4>
        <p>New e-mail addresses must be added to the whitelist before they can be used during registeration. Add an e-mail address to the whitelist below.</p>
        {!! Form::open(array('url'=>'/admin/whitelist', 'method'=>'POST')) !!}
        <div class="row">
            <div class="large-2 columns">{!! Form::label('email', 'E-Mail address:', array('class'=>'inline right text-right')) !!}</div>
            <div class="large-7 columns">{!! Form::text('email', Input::old('email'), array()) !!}</div>
            <div class="large-3 columns end">{!! Form::submit('Add E-mail', array('class'=>'button right')) !!}</div>
        </div>
        {!! Form::close() !!}
    </div>

    {!! Form::open(['url'=>'/admin/remwhitelist']) !!}
    <div class="row">

        <table>
            <thead>
            <tr>
                <th>E-mail Address</th>
                <th>Registered</th>
            </tr>
            </thead>
            <tbody>
            @foreach($registrations as $registration)
                <tr>
                    <td>{{ Form::checkbox('email[]', $registration->email, false) }} {{ Form::label( $registration->email, false, ['class' => 'control-label']) }}</td>
                    <td>
                        @if( $registration->getUser() )
                            Yes
                        @else
                            No
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="large-offset-10 one columns">
            {!! Form::submit('Remove Addresses', array('class'=>'button right')) !!}
        </div>
    </div>
    {!! Form::close() !!}
@else
    <p>This system is configured to allow open registration. Whitelisting is not available.</p>
@endif
