<p>System backup is performed every day at {{ Carbon\Carbon::createFromFormat('H:i', config('dispatch.backup_run_time'))->format('H:ia') }}. Old backups are cleaned every day at {{ Carbon\Carbon::createFromFormat('H:i', config('dispatch.backup_clean_time'))->format('H:ia') }}. Backups are kept for {{ config('laravel-backup.cleanup.defaultStrategy.keepAllBackupsForDays') }} days.</p>
@if(count($backups) > 0)
<table>
    <thead>
        <th>Name</th>
        <th>Size</th>
        <th>Last Modified</th>
        <th>Delete</th>
    </thead>
    <tbody>
    @foreach($backups as $name=>$file)
        <tr>
            <td>{{ link_to('admin/download_backup/'.$name, $name) }}</td>
            <td>{{ round(($file['size'] / 1024), 2) }}KB</td>
            <td>{{ $file['last_update'] }}</td>
            <td>{{ link_to('admin/delete_backup/'.$name, 'Delete') }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
@endif

@if($dropbox_setup)
<h4>Run Backups</h4>
<p>The backup process can be run manually. <a href="/admin/backup">Click here</a> to run the backup process right now. NOTE: the backup process may take several minutes depending on the size of the files being backed up.</p>
@endif