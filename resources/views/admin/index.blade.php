@extends('layouts.master')

@section('title', 'Administration')

@section('content')
    <script type="text/javascript">
        $(document).ready(function() {
            $(document).foundation();
        });

    </script>

    <div class="row">
        <div class="large-9 columns end">
            <h2 id="title">System Administration</h2>
            @if (session('message'))
            <div class="callout primary">
                <p>{{ session('message') }}</p>
            </div>
            @endif
        </div>
    </div>

    @include('common.errors')

    <div class="row collapse">
        <div class="large-2 columns">
            <ul class="tabs vertical" id="nav-tabs" data-tabs>
                <li class="tabs-title <?php echo ($tab == "users" ? "is-active" : ""); ?>"><a href="#userspanel" aria-selected="true">Users &amp; Roles</a></li>
                <li class="tabs-title <?php echo ($tab == "registration" ? "is-active" : ""); ?>"><a href="#registrationpanel">Registration</a></li>
                <li class="tabs-title <?php echo ($tab == "logs" ? "is-active" : ""); ?>"><a href="#logpanel">Logs</a></li>
                <li class="tabs-title <?php echo ($tab == "queue" ? "is-active" : ""); ?>"><a href="#queuepanel">Failed Jobs</a></li>
                <li class="tabs-title <?php echo ($tab == "garbage" ? "is-active" : ""); ?>"><a href="#garbagepanel">Garbage Collection</a></li>
                <li class="tabs-title <?php echo ($tab == "backup" ? "is-active" : ""); ?>"><a href="#backuppanel">Backups</a></li>
            </ul>
        </div>
        <div class="large-10 columns end">
            <div class="tabs-content vertical" data-tabs-content="nav-tabs" >
                <div class="tabs-panel <?php echo ($tab == "users" ? "is-active" : ""); ?>" id="userspanel">
                    <h3>Users &amp; Roles</h3>
                    @include('admin.partials.users')
                </div>

                <div class="tabs-panel <?php echo ($tab == "registration" ? "is-active" : ""); ?>" id="registrationpanel">
                    <h3>User Whitelisting</h3>
                    @include('admin.partials.whitelisting')
                </div>

                <div class="tabs-panel <?php echo ($tab == "logs" ? "is-active" : ""); ?>" id="logpanel">
                    <h3>Logs</h3>
                    @include('admin.partials.logs')
                </div>
                <div class="tabs-panel <?php echo ($tab == "queue" ? "is-active" : ""); ?>" id="queuepanel">
                    <h3>Failed Jobs</h3>
                    @include('admin.partials.jobs')
                </div>
                <div class="tabs-panel <?php echo ($tab == "garbage" ? "is-active" : ""); ?>" id="garbagepanel">
                    <h3>Garbage Collection</h3>
                    @include('admin.partials.garbage')
                </div>
                <div class="tabs-panel <?php echo ($tab == "backup" ? "is-active" : ""); ?>" id="backuppanel">
                    <h3>Back ups</h3>
                    @include('admin.partials.backups')
                </div>
            </div>
        </div>
    </div>
@endsection