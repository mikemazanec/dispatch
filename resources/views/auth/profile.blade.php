@extends('layouts.master')

@section('title', 'Registration')

@section('content')
<?php    
echo Form::open(array('url'=>'/auth/profile', 'method'=>'POST'))."\r\n";
echo Form::token()."\r\n";
?>
<h2>User Profile</h2>
<?php

if(isset($message))
{?>
<div class="callout success large-8 end">
    {{ $message }}
    <a href="" class="close">&times;</a>
</div>
<?php
}
?>

@include('common.errors')

<p>Correct any information that may have changed since your account was created. <strong>ALL fields are required</strong>.</p>

<div class="row prepend-top">
    <div class="large-3 columns">
        <?php echo Form::label('email', 'E-Mail Address:', array('class'=>'inline float-right'));?>
    </div>
    <div class="large-4 columns end">
        <?php echo Form::text('email', $user->email); ?>
    </div>
</div>
<div class="row">
    <div class="large-3 columns">
        <?php echo Form::label('first_name', 'First Name:', array('class'=>'inline float-right'));?>
    </div>
    <div class="large-4 columns end">
        <?php echo Form::text('first_name', $user->first_name); ?>
    </div>
</div>
<div class="row">
    <div class="large-3 columns">
        <?php echo Form::label('last_name', 'Last Name:', array('class'=>'inline float-right'));?>
    </div>
    <div class="large-4 columns end">
        <?php echo Form::text('last_name', $user->last_name); ?>
    </div>
</div>
<div class="row">
    <div class="large-3 columns">
        <?php echo Form::label('timezone', 'Timezone:', array('class'=>'inline float-right'));?>
    </div>
    <div class="large-4 columns end" style="line-height:32px; vertical-align:middle;">
        {!! Form::select('timezone', $tz_list, $user_tz->name) !!}
    </div>
</div>
<div class="row">
    <div class="large-3 columns">
        <?php echo Form::label('', 'Password:', array('class'=>'inline float-right'));?>
    </div>
    <div class="large-4 columns end" style="line-height:32px; vertical-align:middle;">
        <a href="/auth/change_password">Change Password</a>
    </div>
</div>

<div class="row prepend-top">
    <div class="large-offset-3 large-4 columns end">
        <?php echo Form::submit('Save Changes', array('class'=>'button small float-right')); ?>
    </div>
</div>
<?php
echo Form::close();
?>
@endsection
