@extends('layouts.master')

@section('title', 'Login')

@section('content')

    <h2>Account Activation</h2>

    @include('common.errors')
    @include('common.messages')

    <div class="row">
        <div class="large-10 columns">
            <p>Enter the e-mail address you used to register to complete the activation process.</p>
            {{ Form::open(array('url'=>'activate', 'method'=>'POST')) }}
            {{ Form::hidden('code', $code) }}
            <div class="row">
                <div class="large-1 columns">
                    <?php echo Form::label('email', 'E-Mail:', array(
                            'class' => 'inline float-right'));?>
                </div>
                <div class="large-8 columns">
                    <?php echo Form::text('email', '', array());?>
                </div>
                <div class="large-1 columns end">
                    <?php echo Form::submit('Submit', array('class' => 'button small float-right'));?>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection