@extends('layouts.master')

@section('title', 'Registration')

@section('content')

    <script type="text/javascript">
    var RecaptchaOptions = {
        theme : 'clean'
    };
</script>
<?php
echo Form::open(array('url'=>'/auth/register', 'method'=>'POST'))."\r\n";
echo Form::token()."\r\n";
?>
<h2>Register</h2>

@include('common.errors')
@include('common.messages')

<p class="large-10 end">
    Complete the form below. <strong>ALL fields are required</strong>.
</p>

<div class="row prepend-top">
    <div class="large-3 columns">
        <?php echo Form::label('email', 'E-Mail Address:', array('class'=>'inline float-right'));?>
    </div>
    <div class="large-4 columns end">
        <?php echo Form::text('email', $email); ?>
    </div>
</div>
<div class="row">
    <div class="large-3 columns">
        <?php echo Form::label('first_name', 'First Name:', array('class'=>'inline float-right'));?>
    </div>
    <div class="large-4 columns end">
        <?php echo Form::text('first_name', $first_name); ?>
    </div>
</div>
<div class="row">
    <div class="large-3 columns">
        <?php echo Form::label('last_name', 'Last Name:', array('class'=>'inline float-right'));?>
    </div>
    <div class="large-4 columns end">
        <?php echo Form::text('last_name', $last_name); ?>
    </div>
</div>
<div class="row">
    <div class="large-3 columns">
        <?php echo Form::label('', 'Password:', array('class'=>'inline float-right'));?>
    </div>
    <div class="large-4 columns end" style="line-height:32px; vertical-align:middle;">
        <?php echo Form::password('password');?>
    </div>
</div>
<div class="row">
    <div class="large-3 columns">
        <?php echo Form::label('', 'Confirm Password:', array('class'=>'inline float-right'));?>
    </div>
    <div class="large-4 columns end" style="line-height:32px; vertical-align:middle;">
        <?php echo Form::password('confirm_password');?>
    </div>
</div>
<div class="row">
    <div class="large-3 columns">
        <?php echo Form::label('timezone', 'Timezone:', array('class'=>'inline float-right'));?>
    </div>
    <div class="large-4 columns end" style="line-height:32px; vertical-align:middle;">
        {!! Form::select('timezone', $tz_list, $timezone) !!}
    </div>
</div>

<h3>Are You Human?</h3>
<div class="row">
    <p class="large-10 columns end">Please enter the two words displayed below, separated by a single space. If one or more of the words is too difficult to read, click the refresh button <i class="icon-arrows-ccw"></i> below to load a new set of words. If you have difficulty completing the captcha or it isn't displayed, please contact OCHE.</p>
</div>
<div class="row append-bottom">
    <div class="lage-6 columns end">
        <?php
        //echo Recaptcha\Recaptcha::recaptcha_get_html($recaptcha_pub_key, null, true);
        ?>
    </div>
</div>

<div class="row prepend-top">
    <p class="prepend-top ten columns end">By completing the registration process, you are stating that you agree to be bound by the <a href="/terms">OCHE Secure Mail Terms of Service</a> and <a href="http://mus.edu/borpol/bor1300/1302.pdf" target="_blank">Montana University System Privacy Policy</a>.</p>
</div>

<div class="row prepend-top">
    <div class="large-offset-5 large-4 columns end" style="text-align:left;">
        <?php echo Form::submit('Complete Registration', array('class'=>'button small')); ?>
    </div>
</div>
<?php
echo Form::close();
?>
@endsection