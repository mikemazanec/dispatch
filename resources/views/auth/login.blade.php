@extends('layouts.master')

@section('title', 'Login')

@section('content')

    @include('common.errors')
    @include('common.messages')

    <div class="row">
        <div class="large-6 columns" >
            <h2>Send a Message</h2>
            <p>You don't need an account to send a message to an employee of the {{ $org_name }}. <a href='/compose/'>Send a message now</a>.<p/>
            <h2 class='prepend-top'>Receive a Message</h2>
            <p>If you are the recipient of a message from this service, simply click the link in the e-mail notification or copy-and-paste it into your browser. You'll be required to verify your e-mail address before opening the message.</p>
        </div>
        <div class="large-offset-1 large-5 columns">
            <div class="callout secondary">
                <h2>Sign In</h2>
                <p>If you've got an account, sign in here to manage and send messages. Enter your e-mail address and password below.</p>

                <?php
                if(isset($form_errors) || isset($error))
                {
                    ?>
                    <div class="row">
                        <div class="callout alert large-12 columns">
                            <?php
                            if(!empty($form_errors))
                            {
                                foreach($form_errors as $item)
                                {
                                    if(!empty($item))
                                    {
                                        foreach($item as $message)
                                        {
                                            echo "<p>" . $message . "</p>\n";
                                        }
                                    }
                                }
                            }

                            if(isset($error))
                            {
                                echo "<p>" . $error . "</p>\n";
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                }

                echo Form::open(array('url'=>'auth/login', 'method'=>'POST')) . "\r\n";
                echo Form::token() . "\r\n";
                ?>
                <div class="row">
                    <div class="large-3 columns">
                        <?php echo Form::label('email', 'E-Mail:', array(
                            'class' => 'inline float-right'));?>
                    </div>
                    <div class="large-9 columns">
                        <?php echo Form::text('email', '', array());?>
                    </div>
                </div>
                <div class="row">
                    <div class="large-3 columns">
                        <?php echo Form::label('password', 'Password:', array(
                                'class' => 'inline float-right'));?>
                    </div>
                    <div class="large-9 columns">
                        <?php echo Form::password('password', '', array());?>
                    </div>
                </div>
                <div class="row">
                    <div class="large-offset-9 large-3 columns">
                        <?php echo Form::submit('Sign In', array('class' => 'button small float-right'));?>
                    </div>
                </div>
                <p class='prepend-top'>Forgotten your password? <a href='/auth/forgot_password' style='font-weight:bold;'>Reset it</a></p>
                <p class='prepend-top'>Don't have an account? <a href='/auth/register' style='font-weight:bold;'>Sign up now</a></p>
                <?php
                echo Form::close();
                ?>
            </div>
        </div>
    </div>
@endsection