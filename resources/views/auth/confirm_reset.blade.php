@extends('layouts.master')

@section('title', 'Confirm Password Reset')

@section('content')
    {{ Form::open(array('url'=>'/confirm_reset', 'method'=>'POST')) }}
    {{ Form::hidden('code', $code) }}
    <h2>Confirm Password Reset</h2>

    @include('common.errors')
    @include('common.messages')

    <p class="large-10 end">
        Complete the form below. <strong>ALL fields are required</strong>.
    </p>

    <div class="row prepend-top">
        <div class="large-3 columns">
            {{ Form::label('email', 'E-Mail Address:', array('class'=>'inline float-right')) }}
        </div>
        <div class="large-4 columns end">
            {{ Form::text('email', old('email')) }}
        </div>
    </div>

    <div class="row">
        <div class="large-3 columns">
            {{ Form::label('', 'New Password:', array('class'=>'inline float-right')) }}
        </div>
        <div class="large-4 columns end" style="line-height:32px; vertical-align:middle;">
            {{ Form::password('password') }}
        </div>
    </div>
    <div class="row">
        <div class="large-3 columns">
            {{ Form::label('', 'Confirm Password:', array('class'=>'inline float-right')) }}
        </div>
        <div class="large-4 columns end" style="line-height:32px; vertical-align:middle;">
            {{ Form::password('confirm_password') }}
        </div>
    </div>

    <div class="row prepend-top">
        <div class="large-offset-5 large-4 columns end" style="text-align:left;">
            {{ Form::submit('Confirm Reset', array('class'=>'button small')) }}
        </div>
    </div>
    {{ Form::close() }}
@endsection