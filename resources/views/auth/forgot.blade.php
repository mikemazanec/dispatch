@extends('layouts.master')

@section('title', 'Login')

@section('content')

    @include('common.errors')
    @include('common.messages')

    <div class="row">
        <div class="large-10 columns">
            <h2>Forgot Your Password?</h2>
            <p>If you've forgotten your password, enter your e-mail address below. You'll receive an e-mail containing a link to reset your password shortly.</p>
            {{ Form::open(array('url'=>'auth/send_reminder', 'method'=>'POST')) }}
            <div class="row">
                <div class="large-1 columns">
                    {{ Form::label('email', 'E-Mail:', array(
                            'class' => 'inline float-right')) }}
                </div>
                <div class="large-8 columns">
                    {{ Form::text('email', '', array()) }}
                </div>
                <div class="large-1 columns end">
                    {{ Form::submit('Submit', array('class' => 'button small float-right')) }}
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection