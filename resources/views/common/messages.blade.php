@if (session('message'))
    <div class="row">
        <div class="callout success large-8 columns end">
            <p>{{ session('message') }}</p>
            <a href="" class="close">&times;</a>
        </div>
    </div>
@endif