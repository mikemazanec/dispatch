@if(count($errors) > 0)
    <div class="row">
        <div class="callout alert large-8 end">
            @foreach($errors->all() as $error)
            <p> {{ $error }}</p>
            @endforeach
            <a href="" class="close">&times;</a>
        </div>
    </div>
@endif