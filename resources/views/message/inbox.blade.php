@extends('layouts.master')

@section('title', 'Inbox')

@section('content')
    <script>
        $(document).ready(function () {

            function check_empty(container)
            {
                var msg_rows = $('div#'+container).children('div.msg_row');
                if(msg_rows.length == 0)
                {
                    var no_msgs = $('<div class="twelve columns end text-center msg_row">No '+container+' items found</div>');
                    no_msgs.appendTo($('div#'+container));
                }
            }

            $('#delMessages').click(function() {

                if(confirm('Do you really want to mark this message as deleted? You will no longer be able to read the message, but it will be left intact for other recipients or its sender.'))
                {
                    var csrf_token = $("input[name='_token']").val();
                    var messages = $('input.rem_check:checked').map(function(index, element) {
                        return this.value.toString();
                    }).get();

                    $.ajax({
                        url: '/delete',
                        data: {
                            _token: csrf_token,
                            messages: JSON.stringify(messages),
                            expire: 0
                        },
                        type: "POST",
                        dataType: "json",
                        success: function(json)
                        {
                            var messages = json.messages;
                            $.each(json.messages, function(index, value){
                                if(value == true)
                                {
                                    var row = $('input.rem_check[value="' + index + '"]').parent().parent();
                                    //row.css('background-color', '#fce6e2').fadeOut(500, function(){$(this).remove();});
                                    //row.css('background-color', '#fce6e2');
                                    row.remove(); // Need to delay this for the animation...
                                }
                                else {
                                    alert("Message "+index+" couldn't be removed.");
                                }
                            });

                            check_empty('sentMessages');
                            check_empty('recdMessages');
                        },
                        fail: function( xhr, status, errorThrown ){
                            alert('Error: ' + errorThrown);
                        }
                    });
                }
            });
        });
    </script>

    <div class="row">
        <div class="large-9 columns end">
            <h2 id="title">Inbox</h2>
        </div>
    </div>
    <div class="row">

        @include('common.errors')
        @include('common.messages')

        <div class="large-12 columns end">
            @if(isset($recipients_list) && !empty($recipients_list))
                <div class="callout success large-8 end">
                    Notifications have been queued for the following recipients:<br/>
                    @foreach($recipients_list as $recipient)
                        <p>{{ $recipient }}</p>
                    @endforeach
                    <a href="#" class="close" onclick="javascript:close_alert(this);">&times;</a>
                </div>
            @endif

            @if (isset($invalid_recipients) && !empty($invalid_recipients))
                <div class="callout alert large-8 end">
                    The following recipients are not valid e-mail addresses or OCHE employee addresses:<br/>

                    @foreach($invalid_recipients as $invalid_recipient)
                        <p>{{ $invalid_recipient }}</p>
                    @endforeach
                    <a href="#" class="close" onclick="javascript:close_alert(this);">&times;</a>
                </div>
            @endif

            @if (isset($del_message))
                <div class="callout <?php echo($del_message_type == 'fail' ? 'alert' : 'success'); ?> large-8 end">
                    <p>{{ $del_message }}</p>
                    <a href="#" class="close" onclick="javascript:close_alert(this);">&times;</a>
                </div>
            @endif

            {!! Form::open(array('url'=>'#', 'method'=>'post', 'id' => 'rem_form')) !!}

            <div class="row">
                <div class="large-offset-2 large-10 columns">
                    <a href="/compose" class="button hollow secondary small">
                        <i class="icon-mail" style="font-size:1.5em; font-weight:bold;"></i>
                    </a>
                    <a href="/inbox" class="button hollow secondary small">
                        <i class="icon-ccw" style="font-size:1.5em; font-weight:bold;"></i>
                    </a>
                    <a href="#" class="button hollow secondary small" id="delMessages">
                        <i class="icon-trash" style="font-size:1.5em; font-weight:bold;"></i>
                    </a>
                </div>
            </div>

            <div class="row collapse">
                <div class="large-2 columns">
                    <ul class="tabs vertical" id="nav-tabs" data-tabs>
                        <li class="tabs-title <?php echo ($tab == "received" ? "is-active" : ""); ?>"><a href="#receivedpanel" aria-selected="true">Inbox</a></li>
                        <li class="tabs-title <?php echo ($tab == "sent" ? "is-active" : ""); ?>"><a href="#sentpanel">Sent</a></li>
                    </ul>
                </div>
                <div class="large-10 columns end">
                    <div class="tabs-content vertical" data-tabs-content="nav-tabs" >
                        <div class="tabs-panel <?php echo ($tab == "received" ? "is-active" : ""); ?>" id="receivedpanel">
                            <div class="row header">
                                <div class="large-4 columns small-3">
                                    <strong>From</strong>
                                </div>
                                <div class="large-6 columns pull-2 hide-for-small-only">
                                    <strong>Subject</strong>
                                </div>
                                <div class="large-2 columns end small-one push-6">
                                    <strong>Received On</strong>
                                </div>
                            </div>

                            @if( count($received_messages) > 0)
                                @foreach($received_messages as $received)
                                    @if($received->message && !$received->message->trashed() && $received->message->state == 'active')

                                        <div class="row msg_row table_row">
                                            <div class="large-4 columns small-3">
                                                {!! Form::checkbox('rem[]', $received->message->id, false, array('class' => 'rem_check', 'style' => '')) !!}
                                                {{ $received->message->sender->email }}
                                            </div>
                                            <div class="large-6 columns small-four pull-2" style="overflow:hidden; <?php echo(!$received->opened ? 'font-weight:bold;' : '');?>">
                                                @if( $received->replied )
                                                    <i class="icon-reply" style="color:#3D5169;"></i>
                                                @endif
                                                {!! link_to('/message/'.$received->message->id, $received->message->subject) !!}
                                                <span style="color:rgb(140,140,140);">{{ str_limit($received->message->body, 30, $end = "...") }}</span>
                                                @if(count($received->message->files) > 0)
                                                    <i class="icon-attachment"></i>
                                                @endif
                                            </div>
                                            <div class="large-2 columns end small-1 push-6">
                                                {{ date('M d', strtotime($received->message->created_at)) }}
                                            </div>
                                        </div>
                                    @endif
                                @endforeach
                            @else
                                <div class="large-10 columns end text-center msg_row">
                                    No received items found.
                                </div>
                            @endif
                        </div>

                        <div class="tabs-panel <?php echo ($tab == "sent" ? "is-active" : ""); ?>" id="sentpanel">
                            <div class="row header">
                                <div class="large-4 columns small-three">
                                    <strong>To</strong>
                                </div>
                                <div class="large-6 columns pull-2 hide-for-small-only">
                                    <strong>Subject</strong>
                                </div>
                                <div class="large-2 columns end small-1 push-6">
                                    <strong>Sent On</strong>
                                </div>
                            </div>

                            @if( count($sent_messages) > 0)
                                @foreach($sent_messages as $sent)
                                @if( $sent->message )
                                {{-- TODO Add denotation for message statuses --}}

                                <div class="row msg_row table_row">
                                    <div class="large-4 columns small-3">
                                        {!! Form::checkbox('rem[]', $sent->message->id, false, array('class' => 'rem_check', 'style' => '')) !!}
                                        @if (count($sent->message->recipients) > 1)
                                            {{ $sent->message->recipients[0]->email."+" }}
                                        @elseif (count($sent->message->recipients) == 1)
                                            {{ $sent->message->recipients[0]->email }}
                                        @else
                                            <em>error getting recipients</em>
                                        @endif
                                    </div>
                                    <div class="large-6 columns small-4 pull-2" style="overflow:hidden;">
                                        {!! link_to('/message/'.$sent->message->id, $sent->message->subject) !!}
                                        <span style="color:rgb(140,140,140);">{{ substr($sent->message->body, 0, 30) . "..." }}</span>

                                        @if(count($sent->message->files) > 0)
                                            <i class="icon-attachment"></i>
                                        @endif
                                    </div>
                                    <div class="large-2 columns end small-1 push-6">
                                        {{ date('M d', strtotime($sent->message->created_at)) }}
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            @else
                                <div class="large-12 columns end text-center msg_row">
                                    No sent items found.
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection