@extends('layouts.master')

@section('content')
<h2>Message Not Found</h2>
The message was not found. The sender may have removed it, or it may have expired.
@endsection