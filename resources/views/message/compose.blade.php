@extends('layouts.master')

@section('title', 'Inbox')

@section('content')

    <script type="text/javascript">
        $(document).ready(function() {
            @if($type != "reply")
            $(".to").select2({
                placeholder: "Enter or select at least one recipient",
                tags: true,
                tokenSeparators: [',', ' ']
            });
            @endif

          $('#add_file').on( 'click', function( event ) {
              $('<input>').attr('type','file').attr('name', 'attachments[]').appendTo('#file_container');
          });
        });
    </script>

  {!! Form::open(array('url'=>'/send/', 'method'=>'POST', 'files'=>true)) !!}
  @if($type == 'reply')
    {!! Form::hidden('parent_id', $message->id) !!}
  @endif
  {!! Form::token() !!}

  <div class="row">
    <div class="large-9 columns end">
      <h2>Compose Message</h2>
    </div>
  </div>
  <div class="row">
    <div class="large-12 columns end">
      <a href="/inbox" class="button hollow secondary small">
        <i class="icon-level-up" style="font-size:1.5em; font-weight:bold;"></i>
      </a>
    </div>
    <?php
      if(is_object($errors))
      {
        $error_messages = $errors->all();
      }
    ?>
    @if(isset($error_messages) && !empty($error_messages) && is_array($error_messages))
      <div class="row">
          <div class="callout alert large-11 columns">
            @foreach($error_messages as $error)
              <p>{{ $error }}</p>
            @endforeach
            <a href="#" class="close" onclick="javascript:close_alert(this);">&times;</a>
          </div>
      </div>
    @endif

    <div class="row">
      <p class="large-11 columns end">To send a secure message, enter the e-mail address(es) of one or more recipients. Suggestions will be presented to
        you as you enter the address.<?php if($type !== 'reply') { ?> Alternatively, click the <i class="icon-user-add" style="font-size:1.0em; cursor:pointer;"></i>
        icon to select recipients from a list of contacts.<?php } ?> Then complete the rest of the form and click the
        <strong>Send</strong> button. Individual files up to 20MB in size and a total of 50MB will be accepted.</p>
    </div>
    <div class="row">

      <div class="large-2 columns">
          <?php echo Form::label('to', 'To:', array('class'=>'inline right text-right'));?>
      </div>
      <div id="recipient_container" class="large-9 columns" style="padding-bottom:10px;">
        <?php
        switch($type)
        {
        case "reply":
          echo '<span style="display:block; line-height:32px; vertical-align:middle">'.$to_name.'</span>';
          break;

        default:
        ?>

        <select name="to[]" id="to" class="to" multiple="true">
          @foreach($contacts as $index=>$contact)
             <option value="{{ $index }}" {{ count(Input::old('to')) > 0 && in_array($index, Input::old('to')) ? 'selected="selected"' : '' }}>{{ $contact }}</option>
          @endforeach
        </select>

        <?php
        break;
        }
        ?>
      </div>
      <div class="large-1 columns end" style="margin-top:7px;">
        <?php if(($type !== 'reply') && (!empty($bpdc_employees) || !empty($contacts)))
        { ?>
        <i id="add_contact" class="icon-user-add" style="font-size:1.5em; cursor:pointer;"></i>
        <?php } ?>
      </div>
    </div>
    <div class="row">
      <div class="large-2 columns">
        {!! Form::label('subject', 'Subject:', array('class'=>'inline right text-right')) !!}
      </div>
      <div class="large-9 nine columns end">
        <?php
        switch($type){

          case "reply":
            $subject = "Re: ".e($message->subject);
            break;

          /**
           * Unused in this version of the app
           **/
          //case "forward":
          //  $subject = "Fwd: ".e(Crypt::decrypt($message->subject));
          //break;

          default:
            $subject = (Input::old('subject') ? Input::old('subject') : '');
            break;
        }
        echo Form::text('subject', $subject, array());
        ?>
      </div>
    </div>
    <div class="row end append-bottom">
      <div class="large-2 columns">
        <?php echo Form::label('attachments', 'Attachments:', array('class'=>'right inline text-right'));?>

      </div>
      <div class="large-5 columns end">
        <div class="row">
          <div class="large-12 columns end" id="file_container">
            {{ Form::file('attachments[]') }}<br />
          </div>
        </div>
        <a href="#" class="" id="add_file" style="cursor:pointer; font-size:0.8em;"><i class="icon-attach"></i>Add Attachment</a>
      </div>
    </div>
    <div class="row prepend-top end">
      <div class="large-2 columns">
        <?php echo Form::label('body', 'Message Text:', array('class'=>'right inline text-right'));?>
      </div>
      <div class="large-9 columns end">
        <?php

        echo Form::textarea('body', ($type == 'reply' || $type == 'forward' ? chr(10).chr(10).'----- '.$message->sender->email.' wrote on '.$message->created_at->timezone($user_timezone).' -----'.chr(10).e(($message->body)) : (Input::old('body') ? Input::old('body'): '')), array('id'=>'body')); ?>
      </div>
    </div>
    <div class="row prepend-top end">
      <div class="large-offset-8 three columns end">
          <fieldset>
          {{ Form::checkbox('receipt', 'Yes', '') }} {{ Form::label('receipt', 'Request Read Receipt') }}
          </fieldset>
      </div>
    </div>
    <div class="row">
      <div class="large-offset-10 one columns">
        {!! Form::submit('Send', array('class'=>'button right')) !!}
      </div>
    </div>
  </div>
  </div>
  {!! Form::close() !!}
@endsection