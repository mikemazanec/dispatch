@extends('layouts.master')

@section('title', 'Message')

@section('content')
    <style>
        #attachments a{
            padding: 6px 0;
            display:inline-block;
        }

        tr.retrieval_record td {
            font-size:1em;
        }

        li.tip {
            cursor:pointer;
        }

        ul.to_list li {
            font-size:.9em;
        }
    </style>
    <script>
        window.addEvent('domready',function() {

            new FloatingTips('li.tip', {
                position: 'bottom', // Bottom positioned
                center: true,      // Place the tip aligned with target
                arrowSize: 8,
                html: true
            });

            if($('recipient_toggle'))
            {
                $('recipient_toggle').addEvent('click', function(){
                    if($('recipients').getStyle('height') === '<?php echo (count($message->recipients) > 0 ? count($message->recipients) : 0); ?>em')
                    {
                        $('recipients').setStyle('height', '1em');
                        $('recipient_toggle').removeClass('icon-minus-squared');
                        $('recipient_toggle').addClass('icon-plus-squared');
                    }
                    else
                    {
                        $('recipients').setStyle('height', '<?php echo (count($message->recipients) > 0 ? count($message->recipients) : 0); ?>em');
                        $('recipient_toggle').removeClass('icon-plus-squared');
                        $('recipient_toggle').addClass('icon-minus-squared');
                    }
                });
            }

            /**
             * Delete the message
             */
            $('msg_del').addEvent('click', function() {
                if(confirm('Do you really want to mark this message as deleted? You will no longer be able to read the message, but it will be left intact for other recipients or its sender.'))
                {
                    $('rem_form').submit();
                }
            });

            @if($message->sender->email == $user->email)
            /**
             * Expire the message
             */
            $('expire_message').addEvent('click', function(e){
                e.stop();
                if(confirm('Do you really want to expire this message? It will be deleted and will no longer be available to you or any if its recipients. This action is permanent.'))
                {
                    window.location = $('expire_message').getProperty('href');
                }
            });
            @endif
        });
    </script>

    <h2 class="end">{{ $message->subject }}</h2>

    @include('common.errors')
    @include('common.messages')

    {!!  Form::open(array('url'=>'/rem_message', 'method'=>'post', 'id' => 'rem_form')) !!}
    {!! Form::token() . "\r\n" !!}
    {!! Form::hidden('message', $message->id) !!}
    {!! Form::close() !!}

        <div class="button-group right">
            <a href="/inbox<?php echo ($message->sender->email == $user->email ? '/sent' : ''); ?>" class="button hollow secondary small tip"><i class="icon-level-up"></i></a>
            @if($message->sender->email != $user->email)
                <a href="/reply/{{ $message->id }}" class="button hollow secondary small tip"><i class="icon-reply"></i></a>
            @endif

            <a href="/delete/{{ $message->id }}" class="button hollow secondary small tip" id="msg_del"><i class="icon-trash"></i></a>

            @if($user->email == $message->sender->email)
                @if($message->state == 'revoked')
                    <a href="/unlock/{{ $message->id }}" class="button hollow secondary small tip" id="unlock_message"><i class="icon-lock-open"></i></a>
                @elseif($message->state == 'active')
                    <a href="/lock/{{ $message->id }}" class="button hollow secondary small tip" id="lock_message"><i class="icon-lock"></i></a>
                @endif
            @endif
            <a href="/message/{{ $message->id }}/print" class="button hollow secondary small tip"><i class="icon-print"></i></a>
        </div>

    <div class="row">
        <div class="large-8 columns small-3" style="font-size:.9em; color:#555;">
            @if($message->sender->email == $user->email)
                <ul style="list-style:none; display:inline;" class="to_list">
                    <li style="display:inline;">to </li>
                        @for ($i=0; $i<count($message->recipients); $i++)
                        {{  $message->recipients[$i]->email }}
                        @if($i < (count($message->recipients) - 1))
                            ,&nbsp;
                        @endif
                    </li>
                    @endfor
                </ul>

            @else
                from {{ $message->sender->email }}
                <?php $reply = $message->getUserRecipient($user->email); ?>
                @if(!empty($reply) && $reply->replied)
                    <br />replied on {{ \Carbon\Carbon::parse($reply->replied_at)->timezone($user_timezone)->format('h:ia M d, Y') }}<br />
                @endif
            @endif
        </div>
        <div class="large-4 columns end small-1" style="font-size:.9em; color:#555; line-height:1.1em; text-align:right;">
            <strong>{{ ($message->sender->email == $user->email ? "Sent" : "Received") }}:</strong> {{ $message->created_at->timezone($user_timezone)->format('h:ia M d, Y') }}<br />
            <strong>Expires:</strong> {{ $message->created_at->timezone($user_timezone)->addDays($expiration_days)->format('h:ia M d, Y') }}<br />
        </div>
    </div>

    <div class="row">
        <div class="code large-8 columns end prepend-top">{!! nl2br(e($message->body)) !!}<br /><br /></div>
    </div>

    @if($message->files->count() > 0)
    <hr class="prepend-top">
    <div style="font-size:.9em;" class="secondary large-6 columns end append-bottom" id="attachments">
        <h5>Attachment(s):</h5>
        @foreach($message->files->all() as $file)
            @if($file->status == 'ready')
            {{ link_to('/file/'.$file->id, $file->file_name) }} ({{ round(($file->file_size / 1000), 2) }}KB)<br />
            @else
                {{ $file->file_name }} ({{ round(($file->file_size / 1000), 2) }}KB)  <i class="icon-time-slot" style="color:#ffae00; font-weight:bold"></i><br />
            @endif
        @endforeach
    </div>
    @endif

    @if($user->email == $message->sender->email)
    <div style="font-size:.9em;" class="secondary large-6 columns end" id="attachments">
        <h5>Message Activity</h5>
        @if(count($message->retrievals) > 0)
        <table>
            <thead>
            <th>Recipient</th>
            <th>Opened On</th>
            <th>IP Address</th>
            </thead>
            <tbody>
            @foreach($message->retrievals as $retrieval)
                <tr class="retrieval_record">
                    <td>
                        {{ $retrieval->recipient()->email }}
                    </td>
                    <td>
                        {{ $retrieval->created_at->timezone($user_timezone)->format('h:ia M d, Y') }}
                    </td>
                    <td>
                        {{ $retrieval->ip_address }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @else
            This message has not been opened by any recipient(s).
        @endif
    </div>
    @endif
@endsection