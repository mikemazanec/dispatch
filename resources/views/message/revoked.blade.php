@extends('layouts.master')

@section('content')
    <h2>Message Locked</h2>
    The sender has locked this message. Please contact the sender to regain access.
@endsection