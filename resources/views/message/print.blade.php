@extends('layouts.print')

@section('content')
<script>
  window.print();
</script>
<h1>Dispatch Secure Mail</h1>
<hr>

<h3><?php echo e($message->subject); ?></h3>
<strong>From:</strong> <?php echo $message->sender->email; ?><br />
<strong>Sent:</strong> <?php echo $message->created_at; ?><br /><br />

<?php echo e($message->body); ?>

@endsection