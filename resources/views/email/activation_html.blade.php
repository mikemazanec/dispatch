<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <title>Account Activation</title>
    <style type="text/css">
        /* Client-specific Styles */
        #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */
        body{width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
        body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */

        /* Reset Styles */
        body{margin:0; padding:0;}
        img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
        table td{border-collapse:collapse;}
        #backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}

        body, #backgroundTable{
            background-color:#FAFAFA;
        }

        #templateContainer{
        /border: 1px solid #DDDDDD;
        }

        h1, .h1{
            color:#202020;
            display:block;
            font-family:Arial;
            font-size:34px;
            font-weight:bold;
            line-height:100%;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            text-align:left;
        }

        h2, .h2{
            color:#202020;
            display:block;
            font-family:Arial;
            font-size:30px;
            font-weight:bold;
            line-height:100%;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            text-align:left;
        }

        h3, .h3{
            color:#202020;
            display:block;
            font-family:Arial;
            font-size:26px;
            font-weight:bold;
            line-height:100%;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            text-align:left;
        }

        h4, .h4{
            color:#202020;
            display:block;
            font-family:Arial;
            font-size:22px;
            font-weight:bold;
            line-height:100%;
            margin-top:0;
            margin-right:0;
            margin-bottom:10px;
            margin-left:0;
            text-align:left;
        }

        #templatePreheader{
            background-color:#FAFAFA;
        }

        .preheaderContent div{
            color:#505050;
            font-family:Arial;
            font-size:10px;
            line-height:100%;
            text-align:left;
        }


        .preheaderContent div a:link, .preheaderContent div a:visited, /* Yahoo! Mail Override */ .preheaderContent div a .yshortcuts /* Yahoo! Mail Override */{
            color:#336699;
            font-weight:normal;
            text-decoration:underline;
        }

        #templateHeader{
            background-color:#FFFFFF;
            border-bottom:0;
        }

        .headerContent{
            color:#202020;
            font-family:Arial;
            font-size:34px;
            font-weight:bold;
            line-height:100%;
            padding:0;
            text-align:center;
            vertical-align:middle;
        }

        .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
            color:#336699;
            font-weight:normal;
            text-decoration:underline;
        }

        #headerImage{
            height:auto;
            max-width:600px !important;
        }
        #templateContainer, .bodyContent{
            background-color:#FFFFFF;
        }
        .bodyContent div{
            color:#505050;
            font-family:Arial;
            font-size:14px;
            line-height:150%;
            text-align:left;
        }

        .bodyContent div a:link, .bodyContent div a:visited, /* Yahoo! Mail Override */ .bodyContent div a .yshortcuts /* Yahoo! Mail Override */{
            color:#336699;
            font-weight:normal;
            text-decoration:underline;
        }

        .bodyContent img{
            display:inline;
            height:auto;
        }

        #templateFooter{
            background-color:#FFFFFF;
            border-top:0;
        }

        .footerContent div{
            color:#707070;
            font-family:Arial;
            font-size:12px;
            line-height:125%;
            text-align:left;
        }

        .footerContent div a:link, .footerContent div a:visited, /* Yahoo! Mail Override */ .footerContent div a .yshortcuts /* Yahoo! Mail Override */{
            color:#336699;
            font-weight:normal;
            text-decoration:underline;
        }

        .footerContent img{
            display:inline;
        }

        #social{
            background-color:#FAFAFA;
            border:0;
        }

        #social div{
            text-align:center;
        }

        #utility{
            background-color:#FFFFFF;
            border:0;
        }

        #utility div{
            text-align:center;
        }
    </style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<center>
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
        <tr>
            <td align="center" valign="top">
                <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                    <tr>
                        <td align="center" valign="top">
                            <!-- // Begin Template Header \\ -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                <tr>
                                    <td class="headerContent">

                                        <!-- // Begin Module: Standard Header Image \\ -->
                                        <img src="<?php echo Config::get('application.url'); ?>/img/mailheader.gif" style="max-width:600px;" id="headerImage" />
                                        <!-- // End Module: Standard Header Image \\ -->

                                    </td>
                                </tr>
                            </table>
                            <!-- // End Template Header \\ -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <!-- // Begin Template Body \\ -->
                            <table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                <tr>
                                    <td valign="top" class="bodyContent">

                                        <!-- // Begin Module: Standard Content \\ -->
                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top">
                                                    <div>
                                                        <h2 class="h2">Account Activation</h2>
                                                        <p>Thank you for registering with dispatch secure messaging. Click <a href="{{ url('activate/'.$code) }}">here</a> to activate your account or copy and paste the link below into your browser:<br /><br />
                                                            {{ url('activate/'.$code) }}<br />
                                                        </p>
                                                        <p>Do not reply to this message.</p>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // End Module: Standard Content \\ -->

                                    </td>
                                </tr>
                            </table>
                            <!-- // End Template Body \\ -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <!-- // Begin Template Footer \\ -->
                            <table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter">
                                <tr>
                                    <td valign="top" class="footerContent">

                                        <!-- // Begin Module: Standard Footer \\ -->
                                        <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top" width="100%">
                                                    <div>
                                                        <em>Copyright &copy; {{ date('Y') }} {{ config('dispatch.org_name') }}, All rights reserved.</em>

                                                        @if(!empty(config('dispatch.org_address')))
                                                            <br />
                                                            <strong>Our mailing address is:</strong>
                                                            <br />
                                                            {{ config('dispatch.org_name') }}<br />
                                                            {{ config('dispatch.org_address') }}<br />
                                                            {{ config('dispatch.org_city') }}, {{ config('dispatch.org_state') }} {{ config('dispatch.org_zipcode') }}<br />
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // End Module: Standard Footer \\ -->

                                    </td>
                                </tr>
                            </table>
                            <!-- // End Template Footer \\ -->
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
    </table>
</center>
</body>
</html>