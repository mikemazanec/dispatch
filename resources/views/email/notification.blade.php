Dispatch Secure Mail Message Received

You have received a dispatch secure mail message from  {{ $sender }}. Click  <a href="{{ $url }}">here</a>  to view it or copy and paste the following link into your browser:

{{ $url }}

The message expires on {{ $expiration }}.

Do not reply to this message.

Copyright &copy; {{ date('Y') }} {{ config('dispatch.org_name') }}, All rights reserved.

@if(!empty(config('dispatch.org_address')))
    Our mailing address is:
    {{ config('dispatch.org_name') }}
    {{ config('dispatch.org_address') }}
    {{ config('dispatch.org_city') }}, {{ config('dispatch.org_state') }} {{ config('dispatch.org_zipcode') }}
@endif