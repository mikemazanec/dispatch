Password Reset

Click <a href="{{ url('confirm_reset/'.$code) }}">here</a> to reset your password or copy and paste the link below into your browser:

{{ url('confirm_reset/'.$code) }}

Do not reply to this message.

Copyright &copy; {{ date('Y') }} {{ config('dispatch.org_name') }}, All rights reserved.

@if(!empty(config('dispatch.org_address')))
    Our mailing address is:
    {{ config('dispatch.org_name') }}
    {{ config('dispatch.org_address') }}
    {{ config('dispatch.org_city') }}, {{ config('dispatch.org_state') }} {{ config('dispatch.org_zipcode') }}
@endif
