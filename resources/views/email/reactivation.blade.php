Account Reactivation

Your dispatch account has been reinstated. Click <a href="{{ url('activate/'.$code) }}">here</a> to activate your account or copy and paste the link below into your browser:

{{ url('activate/'.$code) }}

Do not reply to this message.

Copyright &copy; {{ date('Y') }} {{ config('dispatch.org_name') }}, All rights reserved.

@if(!empty(config('dispatch.org_address')))
    Our mailing address is:
    {{ config('dispatch.org_name') }}
    {{ config('dispatch.org_address') }}
    {{ config('dispatch.org_city') }}, {{ config('dispatch.org_state') }} {{ config('dispatch.org_zipcode') }}
@endif
