Message Queued For Delivery

Your dispatch secure mail message with subject '{{ $subject }}' has been accepted and queued for delivery to the following recipient(s):
@if(count($recipients) > 0)
    @foreach($recipients as $recipient)
        {{ $recipient }}<br />
    @endforeach
@endif

You can delete this message at any time prior to its expiration date ({{ $expiration }}).

Message reference ID: {{ $reference }}

Do not reply to this message.

Copyright &copy; {{ date('Y') }} {{ config('dispatch.org_name') }}, All rights reserved.

@if(!empty(config('dispatch.org_address')))
    Our mailing address is:
    {{ config('dispatch.org_name') }}
    {{ config('dispatch.org_address') }}
    {{ config('dispatch.org_city') }}, {{ config('dispatch.org_state') }} {{ config('dispatch.org_zipcode') }}
@endif
