Message Receipt Confirmation

Your dispatch secure mail message with subject '{{ $subject }}' ({{ $sent_date }}) was opened by {{ $recipient }} on {{ $open_date }}.

Do not reply to this message.

Copyright &copy; {{ date('Y') }} {{ config('dispatch.org_name') }}, All rights reserved.

@if(!empty(config('dispatch.org_address')))
    Our mailing address is:
    {{ config('dispatch.org_name') }}
    {{ config('dispatch.org_address') }}
    {{ config('dispatch.org_city') }}, {{ config('dispatch.org_state') }} {{ config('dispatch.org_zipcode') }}
@endif

