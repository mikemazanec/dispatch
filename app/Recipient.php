<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipient extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['email','message_id','opened','receipt_sent','replied','replied_at'];
    
    public function message()
    {
        return $this->belongsTo('App\Message');
    }

    public function retrieval()
    {
        return Retrieval::where('recipient_id', '=', $this->id)->first();
    }
}
