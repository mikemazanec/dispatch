<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_Timezone extends Model
{
    public $incrementing = false;
    public $primaryKey = 'user_id';
    public $timestamps = false;
    public $fillable = ['user_id', 'name'];
    protected $table = 'user_timezones';
}
