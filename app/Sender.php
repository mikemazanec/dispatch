<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sender extends Model
{
    use SoftDeletes;

    protected $fillable = ['email','message_id'];
    protected $dates = ['deleted_at'];

    public function message()
    {
        return $this->belongsTo('App\Message');
    }
}
