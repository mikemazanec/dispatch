<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use ParagonIE\Halite;

class GenerateSymKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generatesymkey';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $continue = false;
        if(file_exists(config('dispatch.encryption_key')))
        {
            if($this->confirm('Do you wish to generate the symmetric encrytion key? Creation of a new key will render any existing messages and files unreadable. [y|N]'))
            {
                $continue = true;
            }
        }
        else
        {
            $continue = true;
        }

        if($continue)
        {
            $this->info('Generating a new symmetric encryption key...');
            $enc_key = \ParagonIE\Halite\KeyFactory::generateEncryptionKey();
            \ParagonIE\Halite\KeyFactory::save($enc_key, config('dispatch.encryption_key_path'));
            $this->info('Encryption key generated successfully.');
        }
        else
        {
            $this->info('Symmetric key generation cancelled.');
        }
    }
}
