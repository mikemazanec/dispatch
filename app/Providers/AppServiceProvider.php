<?php

namespace App\Providers;

use Validator;
use Cartalyst\Sentinel;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        Validator::extend('valid_users', function($attribute, $value, $parameters, $validator) {

            if(count($value) > 0)
            {
                foreach($value as $email)
                {
                    $user = \Sentinel::findByCredentials(array(
                        'login' =>  $email
                    ));

                    if(!isset($user) || !is_numeric($user->id)) return false;
                }
            }

            return true;
        });


        Validator::extend('whitelisted',  function($attribute, $value, $parameters, $validator) {

            if((count($value) == 1) && !config('dispatch.allow_open_registration'))
            {
                try
                {
                    $registered = \App\Registration::where('email', '=', $value)->firstOrFail();
                    if($registered && $registered->email == $value)
                    {
                        return true;
                    }
                }
                catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e)
                {
                    return false;
                }
            }
            elseif(config('dispatch.allow_open_registration'))
            {
                return true;
            }

            return false;
        });

        
        Validator::extend('max_file_size',  function($attribute, $value, $parameters, $validator) {

            if(config('dispatch.max_file_size') == 0) return true;

            if($value->getClientSize() > 0 && $value->getClientSize() <= config('dispatch.max_file_size'))
            {
                return true;
            }

            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
