<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Retrieval extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['recipient_id', 'message_id', 'ip_address'];
    
    public function message()
    {
        return $this->belongsTo('App\Message');
    }

    public function recipient()
    {
        return Recipient::where('id', '=', $this->recipient_id)->first();
    }
}
