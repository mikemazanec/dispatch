<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('inbox');
});

Route::group(['middleware'=>'auth'], function(){
    Route::get('/compose', 'messageController@getCompose');
    Route::get('/reply/{id?}', 'messageController@getCompose');
    Route::get('/delete/{id}', 'messageController@getDelete');
    Route::get('/lock/{id}', 'messageController@getLock');
    Route::get('/unlock/{id}', 'messageController@getUnlock');
    Route::get('/inbox/{tab?}', 'messageController@getInbox');
    Route::get('/message/{message_id}/{print?}', 'messageController@getMessage'); // require 2nd segment to be alphanumeric, and optional 3rd segment
    Route::get('/messages/{folder}', 'messageController@messages');
    Route::post('/delete', 'messageController@postDelete');
    Route::post('/expire_message/{id}', 'messageController@postExpire');
    Route::post('/expired', 'messageController@postExpired');
    Route::post('/rem_message', 'messageController@remmessage');
    Route::post('/send/{id?}', 'messageController@postSend');
    
    // File routes
    Route::get('/file/{id}', 'fileController@getFile');

    // Admin Routes
    Route::get('user/{$id}', 'adminController@getUser');
    Route::get('admin', 'adminController@getIndex');
    Route::get('admin/demote/{id}', 'adminController@getDemoteUser');
    Route::get('admin/promote/{id}', 'adminController@getPromoteUser');
    Route::get('admin/activate/{id}', 'adminController@getActivateUser');
    Route::get('admin/deactivate/{id}', 'adminController@getDeactivateUser');
    Route::get('admin/queue_garbage_collection', 'adminController@getCollectGarbage');
    Route::post('admin/rem_registration/{id}', 'adminController@postRemRegister');

    // Queue managment
    Route::get('admin/requeue_job/{id}', 'adminController@getRequeueFailedJob');
    Route::get('admin/delete_job/{id}', 'adminController@getDeleteFailedJob');
    Route::get('admin/requeue_failed_jobs', 'adminController@getRequeueFailedJobs');
    Route::get('admin/delete_failed_jobs', 'adminController@getDeleteFailedJobs');

    // Registration whitelist management
    Route::post('admin/whitelist', 'adminController@postWhitelist');
    Route::post('admin/remwhitelist', 'adminController@postUnWhitelist');

    // User profile management
    Route::get('/auth/profile', 'authController@getProfile');
    Route::post('/auth/profile', 'authController@postProfile');

    // Backup Management
    Route::get('admin/backup', 'adminController@getBackupNow');
    Route::get('admin/download_backup/{file}', 'adminController@getDownloadBackup');
    Route::get('admin/delete_backup/{file}', 'adminController@getDeleteBackup');
});

// Message routes
Route::post('/auth/register', 'authController@postRegister');

Route::get('/auth/login', 'authController@getLogin');
Route::get('/auth/logout', 'authController@getLogout');
Route::get('/auth/register', 'authController@getRegister');
Route::get('/auth/forgot_password', 'authController@getForgotPassword');
Route::get('/activate/{code}', 'authController@getActivate');
Route::get('/confirm_reset/{code}', 'authController@getResetPassword');

Route::post('/activate', 'authController@postActivate');
Route::post('/confirm_reset', 'authController@postResetPassword');
Route::post('/auth/login', 'authController@postLogin');
Route::post('/auth/send_reminder', 'authController@postSendReminder');

//Test route
Route::get('/test', 'messageController@getTest');

