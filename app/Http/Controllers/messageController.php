<?php

namespace App\Http\Controllers;

use App\User_Timezone;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Message;
use App\File;
use App\Recipient;
use App\Retrieval;
use App\Sender;
use Cartalyst\Sentinel;
use Illuminate\Support\MessageBag; 
use Storage;
use Log;
use Validator;
use Mail;
use App\Jobs\ProcessFileUpload;


class messageController extends Controller
{

    public $layout; // All data that gets passed to views
    private $data;
    private $expiration_days;
    private $sender;
    private $datetime;
    private $storage_path;
    private $user;
    private $roles;

    public function __construct()
    {

        $this->datetime = Carbon::now();
        $this->datetime->timezone = new \DateTimeZone('UTC');
        $this->data = array();
        $this->data['tz'] = $this->datetime->tz;
        $this->data['org_name'] = config('dispatch.org_name');
        $this->expiration_days = config('dispatch.expiration_days');
        $this->sender = config('dispatch.sender');
        $this->roles = config('dispatch.roles');
        $this->storage_path = config('dispatch.storage_path');
        $this->user = \Sentinel::check();
        $this->assets = array(
            'css'   =>  array(),
            'js'    =>  array()
        );

        $user_tz = User_Timezone::find($this->user->id);
        $this->data['user_timezone'] = $user_tz->name;
    }

    /**
     * Display the message composition view
     *
     * @param Request $request
     * @param string $id
     * @return mixed
     */
    public function getCompose(Request $request, $id = false)
    {
        $this->data['user'] = $this->user;
        $this->assets['js'][] = "js/select2.full.min.js";
        $this->assets['css'][] = "css/select2.min.css";

        $type = false;

        // Eventually will use a method parameter for the 'forward' type
        if($id)
        {
            $type = "reply";
            $message = Message::find($id);

            $access = false;
            if(!$message)
            {
                return view('message.not_found', $this->data);
            }
            else
            {
                $this->data['message'] = $message;

                // Make sure the user can access it... Either have to be the sender
                // or in the recipient list
                $access = $message->has_access($this->user->email);
            }

            $this->data['to'] = $message->sender->email;
            $sender_user = \Sentinel::findByCredentials(['login'=>$message->sender->email]);
            $this->data['to_name'] = $sender_user->last_name.", ".$sender_user->first_name;
            if(!$access)
            {
                return redirect('/auth/access_denied');
            }
        }

        $contacts = array();
        if(!empty($users))
        {
            foreach($users as $contact)
            {
                if(!array_key_exists($contact->email, $contacts) && ($contact->email != $this->user->email))
                {
                    $contacts[$contact->email] = $contact->first_name." ".$contact->last_name;
                }
            }
        }

        // Get contacts
        $user_role = \Sentinel::findRoleBySlug($this->roles['subscribers']);
        $this->data['contacts'] = [];
        if(count($user_role->users) > 0)
        {
            foreach($user_role->users as $user)
            {
                $this->data['contacts'][$user->email] = $user->last_name.", ".$user->first_name;
            }
        }
        asort($this->data['contacts']);

        $this->data['type'] = $type;
        $this->data['assets'] = $this->assets;
        return view('message.compose', $this->data);
    }

    /**
     * Mark a message for deletion
     *
     * @param string $id
     * @return mixed
     */
    public function getDelete($id = false)
    {
        if(!$id) return view('message.not_found');

        $message = Message::find($id);
        if($message && $message->has_access($this->user->email))
        {
            if($message->sender->email == $this->user->email)
            {
                $message->status = 'revoked';
                $message->save();
                $message->delete();

            }
            else
            {
                $recipient = $message->getUserRecipient($this->user->email);
                $recipient->delete();
            }
        }
        else
        {
            return view('message.not_found');
        }

        return redirect('/inbox')->with('message', 'The message has been marked for deletion.');
    }

    /**
     * Display a users inbox
     *
     * @param Request $request
     * @param string $tab
     * @return mixed
     */
    public function getInbox(Request $request, $tab = 'inbox')
    {
        $this->data['app_key'] = config('app.key');
        $current_time = $this->datetime;
        $expire_date = $current_time->subDays($this->expiration_days);
        $this->data['sent_messages'] = Sender::where('email', '=', $this->user->email)
                                            ->where('created_at', '>', $expire_date)
                                            ->orderBy('created_at', 'desc')->get();
        $this->data['received_messages'] = Recipient::where('email', '=', $this->user->email)
                                            ->where('created_at', '>', $expire_date)
                                            ->orderBy('created_at', 'desc')->get();

        // Get recipient list from previously sent message if they exist
        if($request->session()->get('recipients'))
        {
            $this->data['recipients_list'] = unserialize(Session::get('recipients'));
            Session::forget('recipients');
        }

        if($request->session()->get('invalid_recipients'))
        {
            $this->data['invalid_recipients'] = unserialize(Session::get('invalid_recipients'));
            Session::forget('invalid_recipients');
        }
        $this->data['user'] = $this->user;
        $this->data['tab'] = $tab;
        $this->data['del_message'] = $request->session()->get('del_message');
        $this->data['del_message_type'] = $request->session()->get('del_message_type');
        $this->assets['js'][] = "js/foundation.tabs.js";
        $this->data['assets'] = $this->assets;
        $this->data['tab'] = (session('tab') ? session('tab') : 'received');
        return view('message.inbox', $this->data);
    }

    /**
     * Allow the sender of a message to lock a message
     *
     * @param $id
     */
    public function getLock($id = false)
    {
        if(!$id) return redirect('/message/not_found');

        $message = Message::find($id);
        if(!$message)
        {
            Log::debug($this->user->email." attempted to lock message ".$id." but the message couldn't be found.");
            return view('message.not_found');
        }
        elseif($message->sender->email == $this->user->email)
        {
            $message->state = "revoked";
            $message->save();

            Log::info("Message ".$id." access locked by sender.");
            
            return back()->with('message', 'Message access is revoked.');
        }
        else
        {
            return view('auth.access_denied');
        }
    }

    /**
     * Get a specified message
     *
     * @param Request $request
     * @param $id
     * @param bool $print
     * @return mixed
     */
    public function getMessage(Request $request, $id, $print = false)
    {

        $message = Message::find($id);

        if(!$message)
        {
            return view('message.not_found');
        }
        elseif($message->has_access($this->user->email))
        {
            // Perform recipient functions
            if($this->user->email != $message->sender->email)
            {

                // If access is revoked, display the revoked view.
                if($message->state == 'revoked')
                {
                    Log::info($this->user->email.' attempted to access message '.$id.'. Access was revoked by the sender.');
                    return view('message.revoked', $this->data);
                }
                else
                {

                    Log::info($this->user->email.' received message '.$id);

                    $recipient = $message->getUserRecipient($this->user->email);

                    // mark the message as read
                    if(!$recipient->opened)
                    {
                        $recipient->opened = true;
                        $recipient->save();
                    }

                    // Send the notification if it hasn't already and message->read_receipt was requested

                    if($message->read_receipt && !$recipient->receipt_sent)
                    {
                        $message_data = array(
                            'open_date' => $recipient->updated_at->format('Y-m-d H:i'),
                            'sent_date' => $message->created_at->format('Y-m-d H:i'),
                            'recipient' => $recipient->email,
                            'subject' => $message->subject,
                            'sender' => $message->sender->email
                        );

                        $this->__send_receipt($message_data);
                        $recipient->receipt_sent = true;
                        $recipient->save();
                    }

                    // Add a retrieval record
                    Retrieval::create([
                        'recipient_id' => $recipient->id,
                        'ip_address' => $request->getClientIp(),
                        'message_id' => $message->id
                    ]);
                }
            }
            else
            {
                // Notify the sender that the message has been locked.
                if($message->state == 'revoked')
                {
                    $this->data['errors'] = new MessageBag();
                    $this->data['errors']->add('revoked', 'This message is locked for recipients. Click the unlock button to unlock it.');
                }
            }

            $this->data['message'] = $message;
            $this->data['user'] = $this->user;
            $this->data['expiration_days'] = $this->expiration_days;
            
            return view((!$print ? 'message.message' : 'message.print'), $this->data);
        }
        else
        {
            return view('message.not_found', $this->data);
        }
    }

    /**
     * Allow the sender of a message to unlock a message
     *
     * @param $id
     */
    public function getUnlock($id = false)
    {
        if(!$id)
        {
            return redirect('/message/not_found');
        }

        $message = Message::find($id);
        if(!$message)
        {
            return view('message.not_found');
        }
        elseif($message->sender->email == $this->user->email)
        {
            $message->state = "active";
            $message->save();

            Log::info("Message ".$id." access reinstated by sender.");

            return back()->with('message', 'Message access is reinstated.');
        }
        else
        {
            return view('auth.access_denied');
        }
    }

    /**
     * Accept an ajax request with message ids in json format for soft deletion.
     *
     * @param Request $request
     * @return json
     */
    public function postDelete(Request $request)
    {

        $rules = array(
            'messages'  =>  'required|json'
        );

        $validator = Validator::make($request->all(), $rules);
        $result = array();
        if($validator->fails())
        {
            return response('Invalid Request', 400);
        }
        else
        {
            $messages = json_decode($request->input('messages'), true);
            foreach($messages as $message_id)
            {
                $message = Message::find($message_id);
                if(!$message)
                {
                    $result[$message_id] = false;
                }
                else
                {
                    if($message->has_access($this->user->email))
                    {
                        if($message->sender->email == $this->user->email)
                        {
                            $message->state = 'revoked';
                            $message->save();
                            $message->delete();
                            $result[$message_id] = true;
                            Log::info('Sender '.$this->user->email.' marked message '.$message_id.' as deleted. Record to be deleted during next garbage collection');
                        }
                        else
                        {
                            $recipient = $message->getUserRecipient($this->user->email);
                            $recipient->delete();
                            $result[$message_id] = true;
                            Log::info('Receipient '.$this->user->email.' marked message '.$message_id.' as deleted. Record to be deleted during next garbage collection');
                        }
                    }
                }

            }

            return response()->json(['result' => true, 'messages' => $result]);
        }
    }

    /**
     * Process and send the message
     *
     * @param Request $request
     * @param bool $type
     * @return $this
     * @throws \Exception
     */
    public function postSend(Request $request, $type = false)
    {

        $rules = [
            'body'      => 'required',
            'subject'   => 'required'
        ];

        $parent_message = false;
        if($request->input('parent_id'))
        {
            $parent_message = Message::find($request->input('parent_id'));

            if(!$parent_message)
            {
                return redirect('/not_found');
            }
        }
        else
        {
            $rules['to'] = 'array|valid_users|required';
        }

        /**
         * Validate file uploads
         *
         * Check for: file size limitations and valid mimes set in dispatch.accepted_mimes
         */
        $attachments = [];
        if(count($request->input('attachments') > 0))
        {
            $attachments = $request->file('attachments');
            $attachment_count = count($attachments);
            for($i=0; $i < $attachment_count; $i++)
            {
                $rules['attachments.'.$i] = 'max_file_size|mimes:'.config('dispatch.accepted_mimes');
            }
        }

        // Set validation messages
        $messages = array(
            'body.required' =>  'The message text is required.',
            'valid_users'   => 'One or more recipient addresses is not a valid user.',
            'mimes'    => 'One or more files were not of acceptable type ('.config('dispatch.accepted_mimes').').',
            'max_file_size' => 'One or more files exceeded the maximum file size.',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        // Return to the composition view if it doesn't validate
        if($validator->fails())
        {
            if($parent_message)
            {
                return redirect('/reply/'.$request->input('parent_id'))
                            ->withInput()
                            ->withErrors($validator->errors()->all());
            }
            else
            {
                return redirect('/compose')->withInput()->withErrors($validator->errors()->all());
            }
        }

        // If not recipients, return to the compose method
        if(!$parent_message && count($request->input('to')) == 0)
        {
            Log::error('No recipients to send message to');
            return redirect('/compose')
                        ->withInput()
                        ->withErrors(array('One or more recipients must be entered. Please try again. If you continue to receive this message, please contact OCHE customer service.'));
        }

        // Set the array or recipients.
        if(!$parent_message)
        {
            $message_recipients = $request->input('to');
        }
        else
        {
            $message_recipients = [0 => $parent_message->sender->email];
        }

        // Build the message record
        $message = Message::create([
            "subject"           => $request->input('subject'),
            "body"              => $request->input('body'),
            "read_receipt"      => ($request->input('receipt') == 'Yes' ? 1 : 0),
            "state"             => 'active',
            "reference"         => str_random(8),
            "parent_message"    => $request->input('parent_message', '')
        ]);

        // save the sender record
        $sender = Sender::create([
            'email'         => $this->user->email,
            'message_id'    => $message->id
        ]);
        

        // Mark the message as replied if it is a reply
        if($parent_message)
        {
            Recipient::where('email', '=', $sender->email)
                    ->where('message_id', '=', $request->input('parent_id'))
                    ->update(['replied'=>1, 'replied_at'=>$this->datetime->format('Y-m-d H:i:s')]);
        }

        // if the message was saved successfully, process the rest of the records and uploads
        if($message)
        {
            // Save recipient records
            foreach($message_recipients as $recipient_email)
            {
                if($recipient_email != "")
                {
                    Recipient::create([
                        "email"         => $recipient_email,
                        "message_id"    => $message->id,
                        "opened"        => false,
                        "receipt_sent"  => false
                    ]);
                }
            }

            /**
             * Process uploads
             *
             * For each upload, a record must be created and the status must be set to waiting - fileController->getFile
             * will check for the status of the file before it will allow the download.
             *
             * After creating the file record, move the file to the storage area and pass the file id to the
             * ProcessFileUpload queue for encryption and removal of the tmep file.
             **/
            if(count($attachments) > 0)
            {
                foreach($attachments as $index=>$attachment)
                {
                    if(!empty($attachment) && $attachment->isValid())
                    {

                        $new_name = $this->__generate_filename();

                        $file = File::create([
                            'message_id'    => $message->id,
                            'file_name'     => $attachment->getClientOriginalName(),
                            'mime'          => $attachment->getMimeType(),
                            'file_size'     => $attachment->getClientSize(),
                            'path'          => $new_name, // the path the file will be found at after processing
                            'status'        => 'waiting'
                        ]);

                        // Move the file from temporary upload dir to the storage path
                        $temp_path = $attachment->move($this->storage_path);

                        // Write the temp file name to the file record
                        $temp_path_array = explode("/", $temp_path);
                        $file->temp_path = $temp_path_array[count($temp_path_array) - 1];
                        $file->update();

                        // Add the file to the encryption queue. Delay it by 2 minutes to ensure upload is complete for large files
                        $process_job = (new ProcessFileUpload($file->id))->delay(60 * 2);
                        $this->dispatch($process_job);
                        unset($process_job);
                    }
                }
            }

            /**
             * Send recipients their message notification
             *
             * Build the data array to send to the mailer queue.
             */
            $send_date = $message->created_at;
            $this->data = array(
                'sender'        => $sender->email,
                'url'           => url('message/' . $message->id),
                'expiration'    => $send_date->addDays($this->expiration_days)->format('Y-m-d H:i'),
                'sent_date'     => $message->created_at->format('Y-m-d H:i'),
                'reference'     => $message->reference,
                'subject'       => $request->input('subject'),
            );

            // Send message notifications
            foreach($message_recipients as $recipient_email)
            {
                if($recipient_email != "")
                {
                    if($this->__send_notification($this->data, $recipient_email))
                    {
                        Log::info('Message #' . $message->id . ' notification sent to ' . $recipient_email);
                    }
                    else
                    {
                        Log::error('Message #' . $message->id . ' notification failed for ' . $recipient_email);
                    }
                }
            }

            /**
             * Send the confirmation to the message sender
             *
             * Build the data array to send to the mailer queue.
             */
            $this->data['recipients'] = $message_recipients;
            if($this->__send_confirmation($this->data))
            {
                Log::info('Message #' . $message->id . ' confirmation sent to ' . $sender->email);
                return redirect('/inbox');
            }
            else
            {
                Log::error('Message #' . $message->id . ' confirmation failed for ' . $sender->email);
                return redirect('/inbox')->withErrors(array('Message confirmation could not be queued to ' . $sender->email));
            }
        }
        else
        {
            // Message save failed....
            Log::error('New message creation failed');
            return redirect('/compose')
                        ->withInput()
                        ->withErrors(array('The message could not be created. Please try again. If you continue to receive this message, please contact OCHE customer service.'));
        }
    }

    /**
     * Generate a file name for a message attachment
     *
     * @return mixed
     */
    private function __generate_filename()
    {
        $file_name = str_random(40);
        if(!Storage::disk('local')->has($file_name))
        {
            return $file_name;
        }

        return $this->__generate_filename();
    }

    /**
     * Send a message confirmation to the sender
     *
     * @param array $data
     * @return mixed
     */
    private function __send_confirmation($data = false)
    {
        return Mail::queue(['email.confirmation_html', 'email.confirmation'], $data, function ($message) use ($data)
        {
            $message->from($this->sender, 'Dispatch No Reply');
            $message->to($data['sender']);
            $message->subject('Dispatch Secure Message Queued: '.$data['subject']);
        });
    }

    /**
     * Send a new message notification to a recipient
     *
     * @param bool $data
     * @param bool $recipient
     * @return mixed
     */
    private function __send_notification($data = false, $recipient = false)
    {
        $data['recipient'] = $recipient;
        return Mail::queue(['email.notification_html', 'email.notification'], $data, function ($message) use ($data)
                {
                    $message->from($this->sender, 'Dispatch No Reply');
                    $message->to($data['recipient']);
                    $message->subject('Dispatch Secure Message: '.$data['subject']);
                });
    }

    /**
     * Send a read receipt to the sender
     *
     * @param array $message_data
     */
    private function __send_receipt($data = false)
    {
        return Mail::queue(['email.receipt_html', 'email.receipt'], $data, function ($message) use ($data)
        {
            $message->from($this->sender, 'Dispatch No Reply');
            $message->to($data['sender']);
            $message->subject('Dispatch Secure Message Opened: '.$data['subject']);
        });

    }
}