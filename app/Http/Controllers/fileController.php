<?php

namespace App\Http\Controllers;

use App\User_Timezone;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\File;
use Cartalyst\Sentinel;
use Illuminate\Support\MessageBag;
use Storage;
use Log;
use Crypt;
use Validator;
use Mail;
use ParagonIE\Halite;

class FileController extends Controller
{
    private $datetime;
    private $user;
    private $encryption_key;

    public function __construct()
    {
        $this->datetime = Carbon::now();
        $this->datetime->timezone = new \DateTimeZone('America/Denver');
        $this->data = array();
        $this->data['tz'] = $this->datetime->tz;
        $this->admin_role = config('dispatch.admin_role');
        $this->user_role = config('dispatch.user_role');
        $this->user = \Sentinel::check();
        $this->encryption_key = \ParagonIE\Halite\KeyFactory::loadEncryptionKey(config('dispatch.encryption_key_path'));
    }

    /**
     * Decrypt and output a file and the necessary headers for the download
     *
     * @param $id
     * @return response
     */
    public function getFile($id)
    {
        $file = File::find($id);

        if(!$file)
        {
            Log::info($this->user->email.' downloaded file '.$id.' but it doesn\'t exist');
            return view('files.not_found');
        }
        elseif($file->message->has_access($this->user->email) && $file->status == 'ready')
        {
            Log::info($this->user->email.' downloaded file '.$file->id);

            // Verify integrity of the file.
            \ParagonIE\Halite\File::decrypt(config('dispatch.storage_path').'/'.$file->path, config('dispatch.storage_path').'/'.$file->path.'_decrypted', $this->encryption_key);
            $content = Storage::disk('local')->get($file->path.'_decrypted');
            $hash = \ParagonIE\Halite\File::checksum(config('dispatch.storage_path').'/'.$file->path.'_decrypted');
            if($hash !== $file->hash)
            {
                Log::error('Integrity of file '.$id.' may be compromised. File hash does not match.');
                return back()->withErrors(['File integrity may have been compromised. The administrator has been notified.']);
            }
            
            $headers = array(
                'Content-Disposition' =>  'attachment; filename="'.$file->file_name.'"',
                'Content-Description' => 'File Transfer',
                'Content-Type' => $file->mime,
                'Content-Transfer-Encoding' => 'binary',
                'Expires' => 0,
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Pragma' => 'public',
                'Content-Length' => $file->file_size
            );
            Storage::disk('local')->delete($file->path.'_decrypted');
            return response($content, 200, $headers);
        }
        else
        {
            Log::error($this->user->email.' attempted to download file '.$file->id.' without access.');
            return view('files.not_found');
        }
    }
}
