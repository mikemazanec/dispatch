<?php

namespace App\Http\Controllers;

use App\User_Timezone;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Validator;
use Log;
use Mail;

class authController extends Controller
{
    protected $roles;
    protected $data;
    private $sender;

    public function __construct()
    {
        $this->data = array();
        $this->data['org_name'] = config('dispatch.org_name');
        $this->roles = config('dispatch.roles');
        $this->sender = config('dispatch.sender');
    }

    public function getIndex()
    {

    }

    public function getAccessDenied()
    {

    }

    public function getActivate(Request $request, $code)
    {
        $this->data['code'] = $code;
        return view('auth.activate', $this->data);
    }

    public function getCancelActivation($email)
    {

    }

    public function getCancelAccount()
    {

    }

    public function getChangePassword()
    {

    }

    public function getForgotPassword()
    {
        return view('auth.forgot', $this->data);
    }

    /**
     * Display the login view
     *
     * @param Request $request
     * @return mixed
     */
    public function getLogin(Request $request)
    {

        if($user = \Sentinel::check())
        {
            return redirect('/auth/profile');
        }
        else
        {
            if(!is_null($request->session()->get('login_error')))
            {
                $this->data['error'] = $request->session()->get('login_error');
            }

            Return view('auth.login', $this->data);
        }
    }

    /**
     * Log out the user and end the session
     *
     * @return mixed
     */
    public function getLogout()
    {
        if($user = \Sentinel::check())
        {
            $user = \Sentinel::getUser();
            \Sentinel::logout($user, true);
            return redirect('/auth/login')->with('message', 'Logged out successfully.');
        }
        else
        {
            return redirect('/auth/login');
        }
    }

    public function getConfirmReset($email, $hash)
    {

    }

    /**
     * Display the profile view
     *
     * @param Request $request
     * @return mixed
     */
    public function getProfile(Request $request)
    {
        if($this->data['user'] = \Sentinel::getUser())
        {
            $this->data['tz_list'] = array_combine(\DateTimeZone::listIdentifiers(), \DateTimeZone::listIdentifiers());
            $this->data['user_tz'] = User_Timezone::find($this->data['user']->id);
            Return view('auth.profile', $this->data);
        }
        else
        {
            return redirect('/auth/login');
        }
    }

    /**
     * Display the registration view
     *
     * @param Request $request
     * @return mixed
     */
    public function getRegister(Request $request)
    {

        $form_data = $request->session()->get('form_data');

        if(!empty($form_data))
        {
            //$form_data = unserialize($form_data);
            $this->data['email'] = $form_data['email'];
            $this->data['first_name'] = $form_data['first_name'];
            $this->data['last_name'] = $form_data['last_name'];
            $this->data['timezone'] = $form_data['timezone'];
        }
        else
        {
            $this->data['email'] = '';
            $this->data['first_name'] = '';
            $this->data['last_name'] = '';
            $this->data['timezone'] = 'UTC';
        }
        $this->data['tz_list'] = array_combine(\DateTimeZone::listIdentifiers(), \DateTimeZone::listIdentifiers());

        Return view('auth.register', $this->data);
    }

    public function getResetPassword(Request $request, $code)
    {
        $this->data['code'] = $code;
        return view('auth.confirm_reset', $this->data);
    }

    public function postActivate(Request $request)
    {
        $rules= [
            'email' => 'required|email',
            'code'  =>  'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        // Return to the composition view if it doesn't validate
        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator->errors()->all());
        }
        else
        {
            $user = \Sentinel::findByCredentials(['login'=>$request->input('email')]);
            if(\Activation::exists($user))
            {
                if(\Activation::complete($user, $request->input('code')))
                {
                    // Add the user to the subscriber role.

                    $role = \Sentinel::findRoleByName($this->roles['subscribers']);
                    if(!$user->inRole($role)) $role->users()->attach($user);

                    // If this is a reactivation, remove the user form the inactive role
                    $inactive_role = \Sentinel::findRoleByName($this->roles['inactive']);
                    if($user->inRole($inactive_role)) $inactive_role->users()->detach($user);

                    Log::info('User account for '.$request->input('email').' activated.');
                    return redirect('auth/login')->with('message', 'Activation was successful. Please login now.');
                }
                else
                {
                    Log::info('User account activation for '.$request->input('email').' failed.');
                    return back()->with('message', 'Activation failed. Please try again.');
                }
            }
            else
            {
                return back()->with('message', 'Activation failed. Your e-mail address may have been entered incorrectly.')->withInput();
            }
        }
    }

    public function postLogin(Request $request)
    {

        if($auth = \Sentinel::authenticate($request->only(['email','password'])))
        {
            return redirect('/inbox');
        }
        else
        {
            return back()->withInput()->withErrors(['Invalid login or password.']);
        }
    }

    public function postProfile(Request $request)
    {
        $rules= [
            'email' => 'required|email',
            'first_name' => 'required|min:2|max:45',
            'last_name' => 'required|min:2|max:45',
            'timezone'  =>  'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        // Return to the composition view if it doesn't validate
        if($validator->fails())
        {
            return redirect('auth/profile')->withInput()->withErrors($validator->errors()->all());
        }
        else
        {
            $user = \Sentinel::getUser();
            $metadata = [
                'email'         =>  $request->input('email'),
                'first_name'    =>  $request->input('first_name'),
                'last_name'     =>  $request->input('last_name')
            ];
            \Sentinel::update($user, $metadata);

            $timezone = User_Timezone::find($user->id);
            $timezone->name = $request->input('timezone');
            $timezone->save();

            return redirect('auth/profile')->with('message', 'Profile successfully updated.');
        }
    }

    public function postRegister(Request $request)
    {

        $rules= [
            'email' => 'required|email|unique:users,email|whitelisted',
            'first_name' => 'required|min:2|max:45',
            'last_name' => 'required|min:2|max:45',
            'password' => 'required|min:8',
            'confirm_password' => 'required|same:password',
            'timezone' => 'required',
        ];

        $messages = array(
            'whitelisted' =>  'This e-mail address has not been whitelisted for registration.'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        // Return to the composition view if it doesn't validate
        if($validator->fails())
        {
            return redirect('auth/register')->withInput()->withErrors($validator);
        }
        else
        {
            if($user = \Sentinel::register($request->only(['email', 'password', 'last_name', 'first_name'])))
            {
                $user = \Sentinel::findByCredentials(['login'=>$request->input('email')]);

                // Add the user timezone
                User_Timezone::create([
                    'user_id'   =>  $user->id,
                    'name'      =>  $request->input('timezone')
                ]);

                // Set the activation record and queue the e-mail
                $activation = \Activation::create($user);
                if($this->__send_activation(['email'=>$user->email, 'code'=>$activation->code]))
                {
                    Log::info('Account activation message sent to '.$user->email);
                }
                else
                {
                    Log::error('Account activation message to '.$user->email.' failed');
                }

                return redirect('/auth/login')->with('message', 'Registration was successful. Please activate your account and then login.');
            }
            else
            {
                return back()->withInput()->withErrors(['An error occurred while registering.']);
            }
        }
    }

    public function postResetPassword(Request $request)
    {
        $rules= [
            'email'             =>  'required|email',
            'code'              =>  'required',
            'password'          =>  'required',
            'confirm_password'  =>  'required|same:password'
        ];

        $validator = Validator::make($request->all(), $rules);

        // Return to the confirmation view if it doesn't validate
        if($validator->fails())
        {
            return back()->withInput()->withErrors($validator->errors()->all());
        }
        else
        {
            $user = \Sentinel::findByCredentials(['login'=>$request->input('email')]);
            if(\Reminder::exists($user))
            {
                if(\Reminder::complete($user, $request->input('code'), $request->input('password')))
                {
                    Log::info('Password reset for '.$request->input('email'));
                    return redirect('auth/login')->with('message', 'Password reset was successful. Please login now.');
                }
                else
                {
                    Log::info('Password reset for '.$request->input('email').' failed.');
                    return back()->withErrors(['Password reset failed. Please try again.']);
                }
            }
            else
            {
                return back()->withErrors(['Password reset failed. Your e-mail address may have been entered incorrectly or the reset token has already been used.'])->withInput();
            }
        }
    }

    public function postSendReminder(Request $request)
    {

        $rules= [
            'email' => 'required|email|exists:users,email'
        ];

        $validator = Validator::make($request->all(), $rules);

        // Return to the composition view if it doesn't validate
        if($validator->fails())
        {
            return redirect('auth/forgot_password')->withInput()->withErrors($validator);
        }
        else
        {
            // Get the user and send the reminder
            $user = \Sentinel::findByCredentials(['login'=>$request->input('email')]);

            if($user)
            {
                // Set the activation record and queue the e-mail
                $reminder = \Reminder::create($user);
                if($this->__send_reminder(['email' => $user->email, 'code' => $reminder->code]))
                {
                    Log::info('Password reset message sent to ' . $user->email);
                }
                else
                {
                    Log::error('Password reset message to ' . $user->email . ' failed');
                }

                return redirect('/auth/login')->with('message', 'Check your e-mail for your password rest code.');
            }
            else
            {
                return redirect('/auth/forget_password')->withErrors(['User account could not be found. Please re-enter your e-mail address.']);
            }
        }
    }

    public function portVerified()
    {

    }

    public function postVerify()
    {

    }


    /**
     * Send the activation message to the user
     *
     * @param array $data
     * @return mixed
     */
    private function __send_activation($data = false)
    {
        return Mail::queue(['email.activation_html', 'email.activation'], $data, function ($message) use ($data)
        {
            $message->from($this->sender, 'Dispatch No Reply');
            $message->to($data['email']);
            $message->subject('Dispatch Account Activation');
        });
    }

    /**
     * Send a password reminder message to the user
     *
     * @param array $data
     * @return mixed
     */
    private function __send_reminder($data = false)
    {
        return Mail::queue(['email.forgotpassword_html', 'email.forgotpassword'], $data, function ($message) use ($data)
        {
            $message->from($this->sender, 'Dispatch No Reply');
            $message->to($data['email']);
            $message->subject('Dispatch Password Reset');
        });
    }
}
