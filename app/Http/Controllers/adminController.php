<?php

namespace App\Http\Controllers;

use App\Registration;
use Illuminate\Foundation\Providers\ArtisanServiceProvider;
use Illuminate\Http\Request;
use App\User_Timezone;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Cartalyst\Sentinel;
use Illuminate\Support\MessageBag;
use Log;
use Crypt;
use Validator;
use Mail;
use App\Jobs\CleanExpiredData;
use App\Message;
use DB;
use Artisan;
use Storage;

class adminController extends Controller
{

    private $roles;
    public $layout; // All data that gets passed to views
    private $data;
    private $expiration_days;
    private $sender;
    private $datetime;
    private $user;
    private $administrators_role;

    public function __construct()
    {

        $this->user = \Sentinel::check();

        $this->roles = config('dispatch.roles');

        // Only allow Administrative users
        $this->administrators_role = \Sentinel::findRoleByName($this->roles['administrators']);
        if(!\Sentinel::inRole($this->administrators_role))
        {
            Log::alert('User '.$this->user->email.' attempted to access to administration panel.');
            abort(403, 'Unauthorized Access');
        }

        $this->datetime = Carbon::now();
        $this->datetime->timezone = new \DateTimeZone('America/Denver');
        $this->data = array();
        $this->data['tz'] = $this->datetime->tz;
        $this->data['org_name'] = config('dispatch.org_name');
        $this->expiration_days = config('dispatch.expiration_days');
        $this->sender = config('dispatch.sender');
        $this->assets = array(
            'css'   =>  array(),
            'js'    =>  array()
        );

        $user_tz = User_Timezone::find($this->user->id);
        $this->data['user_timezone'] = $user_tz->name;
    }

    /**
     * Activate a user account and send an activation email
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function getActivateUser(Request $request, $id)
    {
        $user = \Sentinel::findById($id);
        if($user)
        {
            if(\Activation::completed($user))
            {
                return redirect('admin')->withErrors(['User account is already activated.']);
            }
            else
            {
                $activation = \Activation::create($user);
                if($this->__send_reactivation(['email'=>$user->email, 'code'=>$activation->code]))
                {
                    Log::info('Account reactivation message sent to '.$user->email);
                    return redirect('admin')->with('message', 'Account reactivated. Reactivation message sent to '.$user->email.'.');
                }
                else
                {
                    Log::error('Account reactivation message to '.$user->email.' failed');
                    return redirect('admin')->withErrors(['Account reactivation message to '.$user->email.' failed']);
                }
            }
        }
        else
        {
            return redirect('admin')->withErrors(['User not found.']);
        }
    }

    public function getBackupNow(Request $request)
    {
        $request->session()->flash('tab', 'backup');
        Artisan::call('backup:run');
        Log::info('Backup process started manually.');
        return redirect('admin')->with('message', 'Backups queued for processing.');
    }

    /**
     * Dispatch the garbage collector
     *
     * @return mixed
     **/
    public function getCollectGarbage(Request $request)
    {
        $this->dispatch(new CleanExpiredData());
        $request->session()->flash('tab', 'garbage');
        Log::info('Garbage collection process queued..');
        return redirect('admin')->with('message', 'Garbage collection process has been queued.');
    }

    /**
     * Deactivate a user account
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function getDeactivateUser(Request $request, $id)
    {
        $user = \Sentinel::findById($id);
        if($user)
        {
            if(\Activation::completed($user))
            {
                // Remove the activation
                \Activation::remove($user);
                Log::info('User account '.$id.' as been deactivated.');

                // Remove their security roles.
                $subscribers_role = \Sentinel::findRoleByName($this->roles['subscribers']);
                if($user->inRole($subscribers_role)) $subscribers_role->users()->detach($user);

                $administrators_role = \Sentinel::findRoleByName($this->roles['administrators']);
                if($user->inRole($administrators_role)) $administrators_role->users()->detach($user);

                // Move them to the inactive role.
                $inactive_role = \Sentinel::findRoleByName($this->roles['inactive']);
                $inactive_role->users()->attach($user);

                // Lastly, logout the user in case they're logged in
                \Sentinel::logout($user);
                
                return redirect('admin')->with('message', 'User account has been deactivated. If the account is ever reinstated, the previous security roles must be reapplied to the account.');
            }
            else
            {
                return redirect('admin')->withErrors(['User account is not activated. The account could not be deactivated.']);
            }
        }
        else
        {
            return redirect('admin')->withErrors(['User not found.']);
        }
    }


    /**
     * Allow the user to delete a backup file.
     *
     * @param Request $request
     * @param $file
     * @return mixed
     */
    public function getDeleteBackup(Request $request, $file)
    {
        $request->session()->flash('tab', 'backup');
        if(Storage::disk('dropbox')->exists($file))
        {
            Log::info('Backup file '.$file.' deleted manually.');
            Storage::disk('dropbox')->delete($file);
            return redirect('admin')->with('message', 'The backup file '.$file.' was deleted manually.');
        }
        else
        {
            return redirect('admin')->withErrors(['The requested backup file could not be found.']);
        }
    }


    /**
     * Delete a specified job from the failed job table
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function getDeleteFailedJob(Request $request, $id)
    {
        $request->session()->flash('tab', 'queue');
        if(is_numeric($id))
        {
            Artisan::call('queue:forget', ['id' => (array) $id]);
            Log::info('Failed job '.$id.' removed manually.');
            return redirect('admin')->with('message', 'Job ' . $id . ' deleted.');
        }
        else
        {
            return redirect('admin')->withErrors(['Job could not be deleted. Passed job ID was not valid.']);
        }
    }

    /**
     * Delete all jobs in the failed job table
     *
     * @param Request $request
     * @return mixed
     */
    public function getDeleteFailedJobs(Request $request)
    {
        $request->session()->flash('tab', 'queue');
        Artisan::call('queue:flush');
        Log::info('Failed jobs removed from the failed_jobs table.');
        return redirect('admin')->with('message', 'All previously failed jobs deleted.');
    }

    /**
     * Demote a user to the subscribers role
     *
     * @param int $id
     * @return mixed
     */
    public function getDemoteUser(Request $request, $id)
    {
        $user = \Sentinel::findUserById($id);
        $request->session()->flash('tab', 'users');
        if($user)
        {
            $administrators_role = \Sentinel::findRoleByName($this->roles['administrators']);
            $administrators_role->users()->detach($user);
            return redirect('admin')->with('message', 'User '.$user->email.' demoted to subscriber.');
        }
        else
        {
            return redirect('admin')->withErrors(['Invalid account requested for demotion.']);
        }
    }

    public function getDownloadBackup(Request $request, $file)
    {
        if(Storage::disk('dropbox')->exists($file))
        {
            Log::info('Backup file '.$file.' downloaded.');

            $content = Storage::disk('dropbox')->get($file);
            $headers = array(
                'Content-Disposition' =>  'attachment; filename="'.$file.'"',
                'Content-Description' => 'File Transfer',
                'Content-Type' => 'application/zip',
                'Content-Transfer-Encoding' => 'binary',
                'Expires' => 0,
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Pragma' => 'public',
                'Content-Length' => Storage::disk('dropbox')->size($file)
            );
            return response($content, 200, $headers);
        }
        else
        {
            $request->session()->flash('tab', 'backup');
            return redirect('admin')->withErrors(['The requested backup file could not be found.']);
        }
    }

    /**
     * Display the administrator dashboard
     *
     * @param Request $request
     * @return mixed
     */
    public function getIndex(Request $request)
    {

        $this->assets['js'][] = "js/foundation.util.keyboard.js";
        $this->assets['js'][] = "js/foundation.util.motion.js";
        $this->assets['js'][] = "js/foundation.accordion.js";
        $this->assets['js'][] = "js/foundation.tabs.js";

        foreach($this->roles as $slug=>$name)
        {
            $this->data['roles'][$slug] = \Sentinel::findRoleBySlug($slug);
        }


        // Check for dropbox backup settings
        $dropbox_token = env('DROPBOX_APP_TOKEN', false);
        $dropbox_secret = env('DROPBOX_APP_SECRET', false);
        $dropbox_setup = true;
        if((strlen($dropbox_secret) != 15 || !$dropbox_secret) ||
            (strlen($dropbox_token) != 64 || !$dropbox_token))
        {
            $dropbox_setup = false;
        }

        if($dropbox_setup)
        {
            $backups = Storage::disk('dropbox')->files();
            $this->data['backups'] = [];
            if(!empty($backups))
            {
                foreach($backups as $file)
                {
                    $this->data['backups'][$file] = [
                        'size' => Storage::disk('dropbox')->size($file),
                        'last_update' => Storage::disk('dropbox')->lastModified($file)
                    ];
                }
            }
        }
        else
        {
            Log::error('Laravel backup dropbox settings have not been added to the .env file.');
            $this->data['errors'] = new MessageBag(); // This may block other errors thrown from other ops
            $this->data['errors']->add('dropbox', 'The Dropbox API secret and token have not been added to the .env configuration file. Please add these configuration items before attempting to backup dispatch.');
            $this->data['backups'] = [];
        }
        $this->data['dropbox_setup'] = $dropbox_setup;
        unset($dropbox_token);
        unset($dropbox_secret);

        $this->data['failed_jobs'] = DB::table('failed_jobs')->get();

        $this->data['registrations'] = Registration::all();
        
        $this->data['assets'] = $this->assets;

        $this->data['tab'] = (session('tab') ? session('tab') : 'users');

        $this->data['expiration_days'] = $this->expiration_days;
        $expire_date = Carbon::now()->subDays($this->expiration_days);
        $this->data['expired_message_count'] = Message::where('created_at', '<', $expire_date)->count();

        return view('admin.index', $this->data);
    }

    /**
     * Promote a user to administrators role
     *
     * @param int $id
     */
    public function getPromoteUser(Request $request, $id)
    {
        $user = \Sentinel::findUserById($id);
        $request->session()->flash('tab', 'users');
        if($user)
        {
            $administrators_role = \Sentinel::findRoleByName($this->roles['administrators']);
            $administrators_role->users()->attach($user);
            return redirect('admin')->with('message', 'User '.$user->email.' promoted to administrator.');
        }
        else
        {
            return redirect('admin')->withErrors(['Invalid account requested for promotion.']);
        }
    }

    /**
     * Requeue a specified failed job
     *
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function getRequeueFailedJob(Request $request, $id)
    {
        $request->session()->flash('tab', 'queue');
        if(is_numeric($id))
        {
            Artisan::call('queue:retry', ['id' => (array) $id]);
            Log::info('Failed job '.$id.' requeued manually.');
            return redirect('admin')->with('message', 'Job ' . $id . ' requeued for processing.');
        }
        else
        {
            return redirect('admin')->withErrors(['Job could not be requeued. Passed job ID was not valid.']);
        }
    }

    /**
     * Requeue all previouslt failed jobs in the failed job table
     *
     * @param Request $request
     * @return mixed
     */
    public function getRequeueFailedJobs(Request $request)
    {
        $request->session()->flash('tab', 'queue');
        Artisan::call('queue:retry', ['id' => (array) 'all']);
        Log::info('Failed jobs requeue.');
        return redirect('admin')->with('message', 'All previously failed jobs requeued for processing.');
    }


    public function postLookup(Request $request)
    {
        
    }

    /**
     * Remove one or more from the registration whitelist
     *
     * @param Request $request
     */
    public function postUnWhitelist(Request $request)
    {
        $rules = [
          'email'   =>  'array|required'
        ];

        $validator = Validator::make($request->all(), $rules);
        $request->session()->flash('tab', 'registration');

        // Return to the whitelist view if it doesn't validate
        if($validator->fails())
        {
            return redirect('admin')->withInput()->withErrors($validator->errors()->all());
        }
        else
        {
            if( count($request->input('email')) > 0 )
            {
                Registration::destroy($request->input('email'));
            }
            return redirect('admin');
        }
    }

    /**
     * Add an e-mail address to the registration whitelist.
     *
     * @param Request $request
     * @return mixed
     */
    public function postWhitelist(Request $request)
    {
        $rules = [
            'email' =>  'required|email|unique:registrations,email'
        ];

        $validator = Validator::make($request->all(), $rules);
        $request->session()->flash('tab', 'registration');

        // Return to the whitelist view if it doesn't validate
        if($validator->fails())
        {

            return redirect('admin')->withInput()->withErrors($validator->errors()->all());
        }
        else
        {
            $registration = new Registration();
            $registration->email = $request->input('email');
            $registration->save();
            return redirect('admin');
        }
    }

    /**
     * Send the reactivation message to the user
     *
     * @param array $data
     * @return mixed
     */
    private function __send_reactivation($data = false)
    {
        return Mail::queue(['email.reactivation_html', 'email.reactivation'], $data, function ($message) use ($data)
        {
            $message->from($this->sender, 'Dispatch No Reply');
            $message->to($data['email']);
            $message->subject('Dispatch Account Reactivated');
        });
    }
}
