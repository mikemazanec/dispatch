<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use ParagonIE\Halite;

class Message extends Model
{
    use SoftDeletes;
    use Uuids;
    protected $dates = ['deleted_at'];
    protected $fillable = ['subject','body','read_receipt','state','reference','parent_message'];
    public $timestamps = true;
    public $incrementing = false;
    private $encryption_key;

    public function files()
    {
        return $this->hasMany('App\File');
    }

    public function sender()
    {
        return $this->hasOne('App\Sender');
    }

    public function recipients()
    {
        return $this->hasMany('App\Recipient');
    }

    public function retrievals()
    {
        return $this->hasMany('App\Retrieval');
    }

    public function parent()
    {
        return Message::where('id', '=', $this->parent_message)->first();
    }

    /**
     * Mutate the body - decrypt it
     *
     * @param $value
     * @throws Halite\Alerts\CannotPerformOperation
     * @throws Halite\Alerts\InvalidMessage
     */
    public function getBodyAttribute($value)
    {
        $this->encryption_key = \ParagonIE\Halite\KeyFactory::loadEncryptionKey(config('dispatch.encryption_key_path'));
        return \ParagonIE\Halite\Symmetric\Crypto::decrypt($value, $this->encryption_key);

    }

    /**
     * Mutate the subject - decrypt it
     *
     * @param $value
     * @throws Halite\Alerts\CannotPerformOperation
     * @throws Halite\Alerts\InvalidMessage
     */
    public function getSubjectAttribute($value)
    {
        $this->encryption_key = \ParagonIE\Halite\KeyFactory::loadEncryptionKey(config('dispatch.encryption_key_path'));
        return \ParagonIE\Halite\Symmetric\Crypto::decrypt($value, $this->encryption_key);
    }

    /**
     * Mutate the body - decrypt it
     *
     * @param $value
     * @throws Halite\Alerts\CannotPerformOperation
     * @throws Halite\Alerts\InvalidMessage
     */
    public function setBodyAttribute($value)
    {
        $this->encryption_key = \ParagonIE\Halite\KeyFactory::loadEncryptionKey(config('dispatch.encryption_key_path'));
        $this->attributes['body'] = \ParagonIE\Halite\Symmetric\Crypto::encrypt($value, $this->encryption_key);

    }

    /**
     * Mutate the subject - decrypt it
     *
     * @param $value
     * @throws Halite\Alerts\CannotPerformOperation
     * @throws Halite\Alerts\InvalidMessage
     */
    public function setSubjectAttribute($value)
    {
        $this->encryption_key = \ParagonIE\Halite\KeyFactory::loadEncryptionKey(config('dispatch.encryption_key_path'));
        $this->attributes['subject'] = \ParagonIE\Halite\Symmetric\Crypto::encrypt($value, $this->encryption_key);
    }

    /**
     * Determine if a user has access to the message. User must either a) be the sender or b) be a recipient,
     * have not deleted the message, and the message not marked as being revoked.
     *
     * @param $user
     * @return bool
     */
    public function has_access($user_email)
    {

        if($this->sender->email == $user_email)
        {
            return true;
        }
        elseif(count($this->recipients) > 0)
        {
            foreach($this->recipients as $recipient)
            {

                if(($recipient->email == $user_email) && (!$recipient->trashed()) && ($this->status != "revoked"))
                {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Get an individual user's recipient record
     *
     * @param string $email
     * @return obj
     */
    public function getUserRecipient($email)
    {
        return Recipient::where('email', '=', $email)->where('message_id', '=', $this->id)->first();
    }
}
