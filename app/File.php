<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class File extends Model
{
    use SoftDeletes;
    use Uuids;

    protected $dates = ['deleted_at'];
    protected $fillable = ['message_id','file_name','mime','file_size','path','status'];
    public $timestamps = true;
    public $incrementing = false;

    public function message()
    {
        return $this->belongsTo('App\Message');
    }
}
