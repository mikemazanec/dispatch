<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\File;
use Storage;
use Log;
use Crypt;
use ParagonIE\Halite;

class ProcessFileUpload extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $file;
    protected $id;
    protected $storage_path;
    private $encryption_key;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $id)
    {
        $this->id = $id;
        $this->storage_path = config('dispatch.storage_path');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->file = File::find($this->id);
        $this->encryption_key = \ParagonIE\Halite\KeyFactory::loadEncryptionKey(config('dispatch.encryption_key_path'));
        // Encrypt and save the file, then delete the temp.
        $hash = \ParagonIE\Halite\File::checksum($this->storage_path.'/'.$this->file->temp_path);
        \ParagonIE\Halite\File::encrypt($this->storage_path.'/'.$this->file->temp_path, $this->storage_path.'/'.$this->file->path, $this->encryption_key);
        Storage::disk('local')->delete($this->file->temp_path);

        // Log the action
        Log::info('File '.$this->file->id.' processed and encrypted. Original file name: '.$this->file->temp_path);

        // Update the file record
        $this->file->hash = $hash;
        $this->file->temp_path = "";
        $this->file->status = "ready";
        $this->file->save();
    }
}
