<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Message;
use App\Sender;
use App\Retrieval;
use App\Recipient;
use App\File;
use Storage;
use Log;
use Carbon\Carbon;

class CleanExpiredData extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $expire_date;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->expire_date = Carbon::now()->subDays(config('dispatch.expiration_days'));
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $messages = Message::where('created_at', '<', $this->expire_date)->get();

        if(count($messages) > 0)
        {
            Log::info('Garbage collector found '.count($messages).' expired messages to remove.');
            foreach($messages as $message)
            {

                // Delete sender
                Sender::where('message_id', '=', $message->id)->forceDelete();

                // Delete retrieval logs
                $retrieval_count = Retrieval::where('message_id', '=', $message->id)->count();
                Retrieval::where('message_id', '=', $message->id)->forceDelete();

                // Delete recipients
                $recipient_count = Recipient::where('message_id', '=', $message->id)->count();
                Recipient::where('message_id', '=', $message->id)->forceDelete();

                // Delete files
                $file_count = File::where('message_id', '=', $message->id)->count();
                $files = File::where('message_id', '=', $message->id)->get();
                if(count($files) > 0)
                {
                    $file_count = count($files);
                    foreach($files as $file)
                    {
                        Storage::disk('local')->delete($file->path);
                        $file->forceDelete();
                    }
                }

                // Finally delete the message itself
                Log::info('Garbage collector removing expired message '.$message->id.' ('.$recipient_count.' recipients, '.$retrieval_count.' retrievals, '.$file_count.' files)');
                $message->forceDelete();
            }
        }
        else
        {
            Log::info('Garbage collector found 0 expired messages to remove.');
        }
    }
}
