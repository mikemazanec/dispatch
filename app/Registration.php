<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cartalyst\Sentinel;

class Registration extends Model
{
    public $incrementing = false;
    public $primaryKey = 'email';

    /**
     * Find an associated user account
     * 
     * @return mixed
     */
    public function getUser()
    {
        return \Sentinel::findUserByCredentials(['login' => $this->email]);
    }
}
